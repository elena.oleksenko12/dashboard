import { Pipe, PipeTransform } from '@angular/core';
import { CurrentDashboard } from 'src/app/feature/dashboard/dashboardModels';
//import 'angular-mocks';

@Pipe({
  name: 'filterBoards'
})
export class FilterBoardsPipe implements PipeTransform {
  transform(items: CurrentDashboard[], search: string): CurrentDashboard[] {
    if (search.length === 0) return items;
    return items.filter(item=>item.name.includes(search));
  }

}
