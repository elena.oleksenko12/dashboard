import { Pipe, PipeTransform } from '@angular/core';
import { CurrentDashboard } from 'src/app/feature/dashboard/dashboardModels';

@Pipe({
  name: 'sortBoards'
})
export class SortBoardsPipe implements PipeTransform {
  transform(locItems: CurrentDashboard[], args: any[]): any {
const sortField = args[0];
let hiLo=true;
if (args[1] === 'desc') {
  hiLo=false; 
} 
if (locItems!==undefined) {
locItems.sort((a: any, b: any) => {
  if (a[sortField] < b[sortField]) {
    if (hiLo) return -1; else return 1;
  } else if (a[sortField] > b[sortField]) {
    if (hiLo) return 1; else return -1;
  } else {
    return 0;
  }
}
);
}
return locItems;
}
}
