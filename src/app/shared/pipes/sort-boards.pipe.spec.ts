import { SortBoardsPipe } from './sort-boards.pipe';
import { dashboardsMock } from 'src/app/mocks/dashboard-mocks';

describe('SortBoardsPipe', () => {
  it('create an instance', () => {
    const pipe = new SortBoardsPipe();
    expect(pipe).toBeTruthy();
  });

    describe('sort by name', () => {
      let pipe:SortBoardsPipe;
  
      beforeEach(() => {
        pipe = new SortBoardsPipe();
      });
      it('should return sorted elements by "name" ascending', () => {
        const query='name';
        let asc='asc';
        let exp=[dashboardsMock[1], dashboardsMock[2], dashboardsMock[3],dashboardsMock[0]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted elements by "name" descending', () => {
        const query='name';
        let asc='desc';
        let exp=[dashboardsMock[0], dashboardsMock[3],dashboardsMock[2], dashboardsMock[1]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted elements by "created date" ascending', () => {
        const query='createdDate';
        let asc='asc';
        let exp=[dashboardsMock[0], dashboardsMock[1],dashboardsMock[2], dashboardsMock[3]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted elements by "created date" descending', () => {
        const query='createdDate';
        let asc='desc';
        let exp=[dashboardsMock[3], dashboardsMock[2], dashboardsMock[1], dashboardsMock[0]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted items by "number of tasks to do" in ascending order', () => {
        const query='todo';
        let asc='asc';
        let exp=[dashboardsMock[0], dashboardsMock[1],dashboardsMock[2], dashboardsMock[3]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted items by "number of tasks to do" in descending order', () => {
        const query='todo';
        let asc='desc';
        let exp=[dashboardsMock[3], dashboardsMock[2],dashboardsMock[1],dashboardsMock[0]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });

      it('should return sorted items by "number of tasks in progress" in ascending order', () => {
        const query='progress';
        let asc='asc';
        let exp=[dashboardsMock[0], dashboardsMock[1],dashboardsMock[2], dashboardsMock[3]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted items by "number of tasks in progress" in descending order', () => {
        const query='progress';
        let asc='desc';
        let exp=[dashboardsMock[3], dashboardsMock[2],dashboardsMock[1],dashboardsMock[0]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });

      it('should return sorted items by "number of tasks done" in ascending order', () => {
        const query='done';
        let asc='asc';
        let exp=[dashboardsMock[0], dashboardsMock[1],dashboardsMock[2], dashboardsMock[3]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
      it('should return sorted items by "number of tasks done" in descending order', () => {
        const query='done';
        let asc='desc';
        let exp=[dashboardsMock[3], dashboardsMock[2],dashboardsMock[1],dashboardsMock[0]];
        let arr:[]=[];
        let tempDashboardsMock = [...arr, ...dashboardsMock];
        expect(pipe.transform(tempDashboardsMock, [query, asc])).toEqual(exp);
      });
    });
})

