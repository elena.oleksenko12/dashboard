import { dashboardsMock } from 'src/app/mocks/dashboard-mocks';
import { FilterBoardsPipe } from './filter-boards.pipe';

describe('FilterBoardsPipe', () => {
  it('create an instance', () => {
    const pipe = new FilterBoardsPipe();
    expect(pipe).toBeTruthy();
  });

  describe('filter by name', () => {
    let pipe:FilterBoardsPipe;

    beforeEach(() => {
      pipe = new FilterBoardsPipe();
    });

    it('should return item which contains "good" in name', () => {
      let arr:[]=[];
      let tempDashboardsMock = [...arr, ...dashboardsMock];
      const query='good';
      let exp=[tempDashboardsMock[1], tempDashboardsMock[2], tempDashboardsMock[3]];
      let res=pipe.transform(dashboardsMock, query);
      expect(res).toEqual(exp)
    });
    it('should return origin array in case of empty query', () => {
      let arr:[]=[];
      let tempDashboardsMock = [...arr, ...dashboardsMock];
      const query = '';
      expect(pipe.transform(tempDashboardsMock, query)).toEqual(tempDashboardsMock);
    });
    it('should return empty array', () => {
      let arr:[]=[];
      let tempDashboardsMock = [...arr, ...dashboardsMock];
      const query = 'dsvn';
      expect(pipe.transform(tempDashboardsMock, query).length).toEqual(0);
    });
  });
});
