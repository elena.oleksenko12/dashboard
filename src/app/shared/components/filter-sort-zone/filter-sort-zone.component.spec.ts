import { EventEmitter, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSortZoneComponent } from './filter-sort-zone.component';

describe('FilterSortZoneComponent', () => {
  let component: FilterSortZoneComponent;
  let fixture: ComponentFixture<FilterSortZoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterSortZoneComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
    fixture = TestBed.createComponent(FilterSortZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should send an event on select', () => {
    const event=spyOn(component.inputDataChange, 'emit');
    let data=component.inputData='name';
    expect(data).toBeTruthy();
  });
  it('should send an event on select asc', () => {
    const event=spyOn(component.SortDirectionChange, 'emit');
    let data=component.inputData='asc';
    expect(data).toBeTruthy();
  });
  it('should send an event on select asc', () => {
    const event=spyOn(component.SortDirectionChange, 'emit');
    let data=component.inputData='asc';
    expect(data).toBeTruthy();
  });
  it('should set the value input', () => {
    const model='some';
    expect(component.onInputChange(model)).toBeFalsy();
  });
  it('should set the value input', () => {
    const model='some';
    expect(component.onChange(model)).toBeFalsy();
  });
  it('should set the value input', () => {
    const model='some';
    expect(component.onSortbyParamChange(model)).toBeFalsy();
  });
  it('should set the value input', () => {
     expect(component.onSortDirectionAsc()).toBeFalsy();
  });
  it('should set the value input', () => {
      expect(component.onSortDirectionDesc()).toBeFalsy();
    });
});
