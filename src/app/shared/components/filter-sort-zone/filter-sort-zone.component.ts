import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-filter-sort-zone',
  templateUrl: './filter-sort-zone.component.html',
  styleUrls: ['./filter-sort-zone.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterSortZoneComponent {
  @Input() inputData:string = "";
  @Output() inputDataChange = new EventEmitter<string>();
  @Input() SortDirection!:string;
  @Input() SortbyParam: string = ""; 
  @Output() SortDirectionChange = new EventEmitter<string>();
  @Output() SortbyParamChange = new EventEmitter<any>();
  constructor(
  ) {}
   
  onInputChange(model: string){   
    this.inputData = model;
    this.inputDataChange.emit(model);
}
onSortbyParamChange(model: any) {
  this.SortbyParam = model;
  this.SortbyParamChange.emit(this.SortbyParam);
}
onChange(SortbyParam:any) {
  SortbyParam=this.SortbyParam;
  this.SortbyParamChange.emit(this.SortbyParam);
}
onSortDirectionAsc() {
  this.SortDirection = 'asc';
  this.SortDirectionChange.emit(this.SortDirection); 
}
onSortDirectionDesc() {
  this.SortDirection = 'desc';
  this.SortDirectionChange.emit(this.SortDirection);
} 
}
