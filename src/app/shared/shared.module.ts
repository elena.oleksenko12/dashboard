import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterSortZoneComponent } from './components/filter-sort-zone/filter-sort-zone.component';
import { FilterBoardsPipe } from './pipes/filter-boards.pipe';
import { SortBoardsPipe } from './pipes/sort-boards.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    FilterSortZoneComponent,
    FilterBoardsPipe,
    SortBoardsPipe
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [FilterSortZoneComponent, FilterBoardsPipe, SortBoardsPipe]
})
export class SharedModule { }
