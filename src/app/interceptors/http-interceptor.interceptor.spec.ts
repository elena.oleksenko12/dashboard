
import { inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthInterceptor } from './http-interceptor.interceptor';

describe('HeaderInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
        },
      ],
    });
  });

//   afterEach(() => {
//     localStorage.removeItem('authToken');
// });


  it('should add Content-type header', 
  inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpTestingController: HttpTestingController) => {
    localStorage.setItem('authToken', '112233gyjgj');
    let authToken=localStorage.getItem('authToken');
    console.log('authToken');
    console.log(authToken);
      let response;
      const headers = new HttpHeaders();
      http.get('/', { headers }).subscribe(res => response = res);
      const req = httpTestingController.expectOne('/');
    if(req.request.headers.get('Authorization')!==null) {
      expect(req.request.headers.get('Authorization')).toEqual(`Bearer ${authToken}`);
    } else {
   expect(req.request.headers.get('Authorization')).toBeFalsy(); 
    }
      req.flush(true);
      httpTestingController.verify(); 
}
  ));

  it('should add Content-type header', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpTestingController: HttpTestingController) => {
      let response;
      const headers = new HttpHeaders();

      http.get('/', { headers }).subscribe(res => response = res);

      const req = httpTestingController.expectOne('/');
      expect(req.request.headers.get('Content-type')).toBeFalsy();

      req.flush(false);
      httpTestingController.verify();
    }
  ));
});







