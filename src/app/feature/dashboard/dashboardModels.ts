export interface Dashboard {
    board: {
        created_by?: string,
        name: string,
        description: string,
        createdDate: string,
        _id: string,
        todo: number,
        progress: number,
        done: number,
        comment: string,
    }
}
export interface CurrentDashboard {
        created_by?: string,
        name: string,
        description: string,
        createdDate: string,
        _id: string,
        todo: number,
        progress: number,
        done: number,
        comment: string,
}
export interface Dashboards {
    boards: {
        created_by?: string,
        name: string,
        description: string,
        createdDate: string,
        _id: string,
        todo: number,
        progress: number,
        done: number,
        comment: string ,
    }
}
      
