import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-roting';
import { AddDashboardComponent } from './components/add-dashboard/add-dashboard.component';
import { RouterModule } from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms'
import { DashboardService } from './services/dashboard.service';
import { UpdateDashboardComponent } from './components/update-dashboard/update-dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { BoardModule } from '../board/board.module';
import { ItemComponent } from './components/item/item.component';

@NgModule({
  declarations: [DashboardComponent,
  AddDashboardComponent,
  UpdateDashboardComponent,
  ItemComponent],
  imports: [
    CommonModule,
   DashboardRoutingModule,
   RouterModule,
   ReactiveFormsModule,
   SharedModule,
   FormsModule,
   BoardModule
  
  ],
  exports: [DashboardComponent, AddDashboardComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
