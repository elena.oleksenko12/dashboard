import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UpdateDashboardComponent } from './update-dashboard.component';
import { dashboardsMockAll } from 'src/app/mocks/dashboard-mocks';
import { DashboardService } from '../../services/dashboard.service';

describe('UpdateDashboardComponent', () => {
  let component: UpdateDashboardComponent;
  let fixture: ComponentFixture<UpdateDashboardComponent>;
  let service: DashboardService;
  let httpController: HttpTestingController;
  const dashboard = dashboardsMockAll.boards[0];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateDashboardComponent ],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should check dashboard name value', () => {
    component.currentItem=dashboard;
     component.nameUpdateValue = {
      name:'hello',
     } 
    expect(component.nameUpdateValue).toEqual({
      name:  dashboard.name,
    })
  });

  it('should mark dashboard as invalid when it has value', () => {
    let name= component.nameValue;
    name='helen';
    expect(name).toBeTruthy();
    fixture.detectChanges();
  });
  it('should mark dashboard as invalid when it has no value', () => {
    let name= component.nameValue;
    name=undefined;
    expect(name).toBeFalsy();
    fixture.detectChanges();
  });

  it('should have update dashboards', () => {
  service = TestBed.inject(DashboardService);
  httpController = TestBed.inject(HttpTestingController);
  let board = {
    created_by: '1',
    name: 'hello',
    description: 'something about hello',
    createdDate: '01.11.2022',
    _id: '1',
    todo: 0,
    progress: 0,
    done: 0,
    comment: 'comments',
  }

    let name= component.nameValue;
    let nameNew ='helen';
    let nameUpdateValue = {
      name: nameNew
    }
    component.updateBoard(nameNew);
    expect(nameNew).toBeTruthy();
  });
});

