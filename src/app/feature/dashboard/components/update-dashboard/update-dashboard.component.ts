import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-update-dashboard',
  templateUrl: './update-dashboard.component.html',
  styleUrls: ['./update-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateDashboardComponent {
  nameUpdateValue!:object;
  @Input() idBoard:any;
  @Input() currentItem:any;
  @Input() modal!: boolean;
  @Input() item!: string; 
  @Output() newItemUpdate = new EventEmitter<object>();
  nameValue:any;
  newBoard!:any;

  constructor(
    public modalService: ModalService,
    private dashboardService: DashboardService,
    private http: HttpClient
  ) { }

  updateBoard(name:string) {
     this.nameUpdateValue={
       name:  name
     }
     if(name!=='' && name.length>=3 && name.trim().length>=3) {
     this.dashboardService.updateDashboard(this.nameUpdateValue, this.modalService.currentItem).subscribe((result) => {
   this.newBoard=result;
       this.newItemUpdate.emit(result);
     this.modalService.closeModalUpdate();
       });
   }
  }
}
