import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CanLoadService } from 'src/app/services/can-load.service';
import { CurrentDashboard } from '../../dashboardModels';
import { DashboardService } from '../../services/dashboard.service';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent {
  @Input() item!:CurrentDashboard;
  state!:boolean;
  @Output() deleteItemEvent = new EventEmitter<CurrentDashboard>();

  constructor(
    public modalService: ModalService,
    private canLogin: CanLoadService,
    private dashboardService: DashboardService,
    private router: Router
  ) {}
  deleteBoard(item:any) {
    this.canLogin.isUserLoggedIn$.subscribe(
      value => {
            this.state = value;
    if(this.state===true) {
      this.dashboardService.deleteDashboard(item._id)
      .subscribe((res:CurrentDashboard)=> {
        this.deleteItemEvent.emit(res);
      },
      );
    } else {
     this.router.navigate(['/login']);
    }   
      });
  }
}
