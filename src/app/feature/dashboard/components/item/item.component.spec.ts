import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
import { ItemComponent } from './item.component';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;
  let routerSpy = {navigate: jasmine.createSpy('navigate')};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemComponent ],
      providers: [
        { provide: DashboardService, Router, useClass: DashboardService,
          useValue: routerSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    component.item = {
      created_by: '1',
        name: 'hello',
        description: 'something about hello',
        createdDate: '01.11.2022',
        _id: '1',
        todo: 0,
        progress: 0,
        done: 0,
        comment: 'comments',
    }
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('The component renders an element using an input', () => {
    component.item = {
      created_by: '1',
        name: 'hello',
        description: 'something about hello',
        createdDate: '01.11.2022',
        _id: '1',
        todo: 0,
        progress: 0,
        done: 0,
        comment: 'comments',
    }
    expect(component.item.name).toBe('hello'); 
  });
});


