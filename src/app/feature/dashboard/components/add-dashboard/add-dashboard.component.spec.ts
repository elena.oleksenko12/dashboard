import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AddDashboardComponent } from './add-dashboard.component';

describe('AddDashboardComponent', () => {
  let component: AddDashboardComponent;
  let fixture: ComponentFixture<AddDashboardComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDashboardComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule],
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should mark dashboard as invalid when it has value', () => {
    let name= component.nameValue;
    name='helen';
    expect(name).toBeTruthy();
    fixture.detectChanges();
  });
  it('should mark dashboard as invalid when it has no value', () => {
    let name= component.nameValue;
    name=undefined;
    expect(name).toBeFalsy();
    fixture.detectChanges();
  });
  it('#should add Board', () => {
    let name='helen';
    let text='some';
    expect(component.addBoard(name, text)).toBeFalsy();
   });
});

