import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { DownloadBoardService } from '../../services/download-board.service';
import { Subscription } from 'rxjs';
import { CurrentDashboard } from '../../dashboardModels';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-add-dashboard',
  templateUrl: './add-dashboard.component.html',
  styleUrls: ['./add-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddDashboardComponent {
subscription!: Subscription;
formData:any;
nameValue: any;
descriptionValue:any;
board:any;
objectData!:CurrentDashboard;
boardArrayCurrent!:any;
boardArrayNew:any;
errorMessage!:string;
@Output() newItemEvent = new EventEmitter<CurrentDashboard>();
@Output() closeMeEvent = new EventEmitter();
@Output() confirmEvent = new EventEmitter();

  constructor(
    private  dashboardService:  DashboardService,
    private downloadService: DownloadBoardService,
    public modalService: ModalService
  ) {}

addBoard(name:any, description: any) {
  this.nameValue=name;
  this.descriptionValue=description;
  this.formData = {
  name: this.nameValue,
        description: this.descriptionValue
  }
  if(name!=='' && description!=='' &&
  name.length>=3 && description.length<=20! &&
  name.trim().length>=3 && description.trim().length<=20
  ) {
 this.dashboardService.addDashboard(this.formData).subscribe((res) => {
      this.objectData = res.board;
      this.newItemEvent.emit(res.board);
      this.modalService.closeModal();
    },
    );
  } 
} 
}




