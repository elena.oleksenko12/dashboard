import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { dashboardsMockAll } from 'src/app/mocks/dashboard-mocks';
import { DashboardService } from '../../services/dashboard.service';
import { DashboardComponent } from './dashboard.component';
import { DashboardServiceMock } from './dashboard.service.mock';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const dashboard = dashboardsMockAll.boards[0];

  beforeEach(async () => {
    const dashboardServiceSpy = jasmine.createSpyObj<DashboardServiceMock>(['getDashboards']);
    dashboardServiceSpy.getDashboards.and.returnValue(of(dashboardsMockAll.boards));
   
    await TestBed.configureTestingModule({
      declarations: [ DashboardComponent, ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule, 
        ],
      providers: [{provide: DashboardService,
        useValue: dashboardServiceSpy,
       },
       ], 
    })
    .compileComponents();
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have new dashboard', () => {
    let board = {
      created_by: '1',
    name: 'hello',
    description: 'something about hello',
    createdDate: '01.11.2022',
    _id: '1',
    todo: 0,
    progress: 0,
    done: 0,
    comment: 'comments'
    }
    expect(component.addItem(board)).toBeFalsy();
  });
  it('should have edit dashboard', () => {
    let board = {
      created_by: '1',
    name: 'hello',
    description: 'something about hello',
    createdDate: '01.11.2022',
    _id: '1',
    todo: 0,
    progress: 0,
    done: 0,
    comment: 'comments'
    }
    expect(component.updateItem(board)).toBeFalsy();
  });
  it('should have delete dashboard', () => {
    let board = {
      created_by: '1',
    name: 'hello',
    description: 'something about hello',
    createdDate: '01.11.2022',
    _id: '1',
    todo: 0,
    progress: 0,
    done: 0,
    comment: 'comments'
    }
    expect(component.deleteItem(board)).toBeFalsy();
  });
});
