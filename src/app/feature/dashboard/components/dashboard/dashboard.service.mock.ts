import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { dashboardsMockAll } from 'src/app/mocks/dashboard-mocks';
import { CurrentDashboard } from '../../dashboardModels';

@Injectable()
export class DashboardServiceMock {
  constructor() {
  }
  getDashboards(): Observable<CurrentDashboard[]> {
    return of(dashboardsMockAll.boards);
  }
  deleteDashboard(id: string): Observable<CurrentDashboard> {
    return of(dashboardsMockAll.boards[0])
  }
  updateDashboard(data: Partial<CurrentDashboard>, id: string) {
    return of(true);
  }
}