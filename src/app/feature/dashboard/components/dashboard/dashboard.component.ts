import { Component, OnInit, OnDestroy, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { CanLoadService } from 'src/app/services/can-load.service';
import { CurrentDashboard } from '../../dashboardModels';
import { DashboardService } from '../../services/dashboard.service';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
input: string = "";
id:any;
items$!: Observable<any>;
arrayIdLocal!:any;
arrayIdStorage:any;
array:any;
state!:boolean;
SortDirection:any;
SortbyParam:string = '';
errorMessage!:string;
nameUpdateValue:any;
formData: any;
objectData!:object;
@Output() newItemEvent = new EventEmitter<object>();
infoAuth: any;
newDashboard:any;
currentItem!: any; 
itemId!: string;
boardsArray:any;
@Input() items!:CurrentDashboard[];

  constructor(
    public modalService: ModalService,
    private dashboardService: DashboardService,
    public fb: FormBuilder,
    ) { 
    this.arrayIdStorage=localStorage.getItem('idArray');
    if(this.arrayIdLocal===null && this.arrayIdStorage.length>0) {     
      this.arrayIdLocal=JSON.parse(this.arrayIdStorage);
    } else {
      this.arrayIdLocal=[];
    } 
  }
ngOnInit(): void {
  this.infoAuth = {
    token: localStorage.getItem('token'),
    user: localStorage.getItem('user')
  }
  this.items$= 
  this.dashboardService.getDashboards()
  .pipe(
      map((res) => {
        return res.boards;
      }),
    );
  }
addItem(newItem:object) {
    this.ngOnInit();
}
public editForm = new FormGroup({
  name: new FormControl('', [Validators.required, Validators.minLength(4)]),
});
updateItem(updateItem: any) {
this.ngOnInit();
}
deleteItem(board: CurrentDashboard) {
  this.itemId=board._id;
   this.ngOnInit();
  }
}



  





 

  
  


  


