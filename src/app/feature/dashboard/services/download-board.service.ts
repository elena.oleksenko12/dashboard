import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Dashboard } from '../dashboardModels';
import { DashboardService } from './dashboard.service';

@Injectable({
  providedIn: 'root'
})
export class DownloadBoardService {
public downloadBoardData$: Observable<any>;
public downloadBoardData$$ = new BehaviorSubject([]);

  constructor(
  ) { 
   this.downloadBoardData$ = this.downloadBoardData$$.asObservable();
  }
  getCurrentState() {
    return this.downloadBoardData$$.getValue();
  }
  setAuthState(value: any) {
    this.downloadBoardData$$.next(value);
  }
}