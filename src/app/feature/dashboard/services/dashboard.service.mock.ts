import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { dashboardsMockAll } from 'src/app/mocks/dashboard-mocks';
import { CurrentDashboard } from '../dashboardModels';

@Injectable()
export class UserServiceMock {
  constructor() {
  }
  getDashboards(): Observable<CurrentDashboard[]> {
    return of(dashboardsMockAll.boards);
  }
  updateDashboard(data: Partial<CurrentDashboard>, id: string) {
    return of(true);
  }
  deleteDashboard(data: Partial<CurrentDashboard>, id: string) {
    return of(true);
  }
  addDashboard(data: Partial<CurrentDashboard>, id: string): Observable<CurrentDashboard> {
    return of(dashboardsMockAll.boards[0])
  }
}