import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DownloadBoardService } from './download-board.service';

describe('DownloadBoardService', () => {
  let service: DownloadBoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DownloadBoardService, HttpClientTestingModule, HttpTestingController,
     ]
    });
    service = TestBed.inject(DownloadBoardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should setAuthState', (() => {
    let value=2;
    service.setAuthState(value);
    service.downloadBoardData$.subscribe(value => {
      expect(value).toBeTruthy();
});
  }));
  it('should return value', (() => {
    service.getCurrentState()
    service.downloadBoardData$.subscribe(value => {
      expect(value).toEqual([]);
});
  }));
});
