import { Injectable, Input } from '@angular/core';
import { catchError, Observable, take, tap, throwError } from 'rxjs';
import { CurrentDashboard, Dashboard } from '../dashboardModels';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ErrorGlobalService } from 'src/app/services/error-global.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
 @Input() dashboard: any;
 dashboardId: any;
items: any[] = [];

  constructor(private http: HttpClient, private errorService: ErrorGlobalService
   ) { }
  addDashboard(model: object): Observable<any> {
  return this.http.post<Dashboard>(`http://localhost:8080/api/dashboards`,  model)
  .pipe(
  tap(
    (res: Dashboard) => {
        }
      ),
      catchError(this.errorHandler.bind(this))
    )   
    }
  deleteDashboard(id: string): Observable<CurrentDashboard> {
  return this.http.delete<CurrentDashboard>(`http://localhost:8080/api/dashboards/${id}`)
  .pipe(
    take(1),
  tap(
    (res: CurrentDashboard) => {
    }
  ),
  catchError(this.errorHandler.bind(this))
  )
    }
  getDashboards(): Observable<any> {
  return this.http.get<any>(`http://localhost:8080/api/dashboards`)
  .pipe(
  tap(
    (res: any) => {
      this.items=res.boards;
    }),
    catchError(this.errorHandler.bind(this))
  )
  }
  updateDashboard(Model:any, id: string): Observable<Dashboard> {
    return this.http.patch<Dashboard>(`http://localhost:8080/api/dashboards/${id}`, Model)
    .pipe(
    tap(
      (res: Dashboard) => {
this.items.push(res.board);
      }),
      catchError(this.errorHandler.bind(this))
    )
    }
    private errorHandler(error:HttpErrorResponse) {
      this.errorService.handle(error.message);
      return throwError(()=>{
        error.message;
      });

    }

}

