import {  Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
currentItem:any;
isModalOpenUpdate:boolean=false;
isModalOpen:boolean=false;

  constructor() {}
  openModalUpdate(item:any) {
    this.isModalOpenUpdate = true;
    this.currentItem=item._id;
  }
  openModal() {
    this.isModalOpen = true;
  }
  closeModalUpdate() {
    this.isModalOpenUpdate = false;
  }
  closeModal() {
    this.isModalOpen = false;
  }
}
