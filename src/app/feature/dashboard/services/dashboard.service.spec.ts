import { HttpClient} from '@angular/common/http';
import { of } from 'rxjs';
import { dashboardsMock, dashboardsMockAll } from 'src/app/mocks/dashboard-mocks';
import { ErrorGlobalService } from 'src/app/services/error-global.service';
import { DashboardService } from './dashboard.service';

describe('DashboardService', () => {

  describe('DashboardService (with spies)', () => {
    let httpClientSpy: jasmine.SpyObj<HttpClient>;
    let service: DashboardService;
  
    beforeEach(() => {
      httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'patch', 'delete']);
      service = new DashboardService(httpClientSpy, new ErrorGlobalService);
    });
  
    it('should return dashboards (HttpClient called once)', (done: DoneFn) => {
      httpClientSpy.get.and.returnValue(of(dashboardsMock));
  
      service.getDashboards().subscribe({
        next: dashboards => {
          expect(dashboards).toEqual(dashboardsMock);
          done();
        },
        error: done.fail
      });
  
      expect(httpClientSpy.get.calls.count()).toBe(1);
    });

  it('should return post dashboard (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(dashboardsMockAll.boards));

    const data = {
      created_by: '1',
        name: 'new',
        description: 'something about new',
        createdDate: '04.11.2022',
        _id: '5',
        todo: 4,
        progress: 4,
        done: 4,
        comment: 'comments',
  }
    service.addDashboard(data).subscribe({
      next: dashboard => {
        expect(dashboard.push(data)).toEqual(dashboardsMockAll.boards.length=5);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.post.calls.count()).toBe(1);
  });

  it('should return patch dashboard (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.patch.and.returnValue(of(dashboardsMockAll.boards[0]));
    let id='1';
    let data={
      name: 'afternoon'
    }
    service.updateDashboard(data,id).subscribe({
      next: dashboard => {
        expect(dashboard).toBeTruthy();
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.patch.calls.count()).toBe(1);
  });

  it('should return delete dashboard (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.delete.and.returnValue(of(dashboardsMockAll.boards[0]));
    let id='1';
    service.deleteDashboard(id).subscribe({
      next: data => {
        expect(data).toEqual(dashboardsMockAll.boards[0]);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1);
  });
});
});



  
  

