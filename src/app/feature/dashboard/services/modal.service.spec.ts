import { TestBed } from '@angular/core/testing';

import { ModalService } from './modal.service';

describe('ModalService', () => {
  let service: ModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should return value', (() => {
    service.closeModal();
      expect(service.isModalOpen).toBe(false);
  }));
  it('should return value11111', (() => {
    service.closeModalUpdate();
      expect(service.isModalOpenUpdate).toBe(false);
  }));
  it('should return value11111', (() => {
    service.openModal();
      expect(service.isModalOpenUpdate).toBe(false);
  }));
  it('should return value11111', (() => {
   let board = {
    created_by: '1',
    name: 'hello',
    description: 'something about hello',
    createdDate: '01.11.2022',
    _id: '1',
    todo: 0,
    progress: 0,
    done: 0,
    comment: 'comments',
   }
    service.openModalUpdate(board);
      expect(service.isModalOpenUpdate).toBe(true);
  }));
});
