export interface Login {
    username: any,
    password: any;
  }

  export interface Registration {
    password: any;
    username: any;
  }

  export interface RegistrationStatus {
    message: string
  } 

  export interface Token {
    jwt_token: string,
    user: {
      _id:string,
      password: string,
      username: string,
      createdDate: string,
      typeColumnTodo: string,
      typeColumnProgress: string,
      typeColumnDone: string,
      listColorTodo: string,
      listColorProgress: string,
      listColorDone: string
    }
  
  }

  export interface User {
    _id: string,
    username: string,
    password: string,
    createdDate: string,
    typeColumnTodo: string,
    typeColumnProgress: string,
    typeColumnDone: string,
    listColorTodo: string,
    listColorProgress: string,
    listColorDone: string
  }
