import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './registration/registration.component';
import {ReactiveFormsModule} from '@angular/forms'
import { AuthService } from './services/auth.service';
import { ChangeStatusService } from 'src/app/core/components/service/change-status.service';

@NgModule({
  declarations: [
    AuthComponent,
    RegistrationComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  providers: [AuthService, ChangeStatusService],
  exports: [AuthComponent, RegistrationComponent]
})
export class AuthModule { }
