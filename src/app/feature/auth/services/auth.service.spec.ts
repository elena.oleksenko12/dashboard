import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ErrorGlobalService } from 'src/app/services/error-global.service';
import { tokenMock, usersMock } from '../auth/users-mock';
import { RegistrationStatus } from '../models';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let authClientSpy: jasmine.SpyObj<HttpClient>;
  let service: AuthService;
  let status: RegistrationStatus = {
    message: 'Profile created successfully'
  }
  beforeEach(() => {
    authClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new AuthService(authClientSpy, new ErrorGlobalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return registration message (HttpClient called once)', (done: DoneFn) => {
    authClientSpy.post.and.returnValue(of(status));
    let data = {
      username: 'jonny5',
      password: '123456aA'
    }
   
    service.registration(data).subscribe({
   
      next: users => {
        expect(users).toEqual(status)
        done();
      },
      error: done.fail
    });

    expect(authClientSpy.post.calls.count()).toBe(1);
  });
  it('should return new user (HttpClient called once)', (done: DoneFn) => {
    authClientSpy.post.and.returnValue(of(tokenMock));

    let user = {
      jwt_token: '123456666',
      user: {
        _id:'5',
        password: '123456dD',
        username: 'jonny5',
        createdDate: '08.11.2022',
        typeColumnTodo: 'todo',
        typeColumnProgress: 'progress',
        typeColumnDone: 'done',
        listColorTodo: 'white',
        listColorProgress: 'pink',
        listColorDone: 'white'
    }
  }
    service.login(user).subscribe({
      next: user => {
 let arr:[]=[];
    let tempusersMock = [...arr, user];
        expect(tempusersMock.push(user)).toEqual(usersMock.length=2);
        done();
      },
      error: 
        done.fail
    });
    expect(authClientSpy.post.calls.count()).toBe(1);
    expect(localStorage.setItem('listColorTodo', user.user.listColorTodo)).toBeFalsy();
  });


});
