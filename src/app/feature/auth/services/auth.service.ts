import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Token, Registration, User, RegistrationStatus } from '../models';
import { Observable, throwError } from 'rxjs';
import {catchError, take, tap} from 'rxjs/operators';
import { ErrorGlobalService } from 'src/app/services/error-global.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser!: User;
  constructor(
    private http: HttpClient,
    private errorService: ErrorGlobalService
  ) { }

  login(model: object): Observable<Token> {
  return this.http.post<Token>(`http://localhost:8080/api/auth/login`,  model)
  .pipe(
  take(1),
  tap(
    (token: Token) => {
      localStorage.setItem('token', token.jwt_token);
      localStorage.setItem('user', JSON.stringify(token.user));
      localStorage.setItem('username', token.user.username);
      localStorage.setItem('listColorTodo', token.user.listColorTodo);
      localStorage.setItem('typeColumnTodo', token.user.typeColumnTodo);
      localStorage.setItem('listColorProgress', token.user.listColorProgress);
      localStorage.setItem('typeColumnProgress', token.user.typeColumnProgress);
      localStorage.setItem('listColorDone', token.user.listColorDone);
      localStorage.setItem('typeColumnDone', token.user.typeColumnDone);
      localStorage.setItem('status', 'true');
    }
  ),
  catchError(this.errorHandler.bind(this))
  )
}

registration(model: Registration): Observable<RegistrationStatus> {
  return this.http.post<RegistrationStatus>(`http://localhost:8080/api/auth/register`,  model)
  .pipe(
    take(1),
    tap(
      (res: RegistrationStatus) => {
      }
    ),
    catchError(this.errorHandler.bind(this))
    )
}

private errorHandler(error:HttpErrorResponse) {
  this.errorService.handle(String(error.status)+error.statusText);
  return throwError(()=>{
    error.message || "Server error"
  });
}
}
