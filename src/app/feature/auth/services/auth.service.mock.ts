
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { tokenMock } from '../auth/users-mock';
import { Registration, Token } from '../models';

@Injectable()
export class AuthService {
  constructor() {
  }

login(model: Partial<Token>) {
    return of(tokenMock.jwt_token);
  }
  registration(model: Partial<Registration>) {
    return of(tokenMock.user);
  }
}