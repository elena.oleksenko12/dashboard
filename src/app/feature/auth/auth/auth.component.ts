import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
//import { ServiceService } from '../../services/service.service';
import { Token, Login, User } from '../models'; 
import {Router} from '@angular/router';
import { AuthService } from '../services/auth.service';
import { CanLoadService } from 'src/app/services/can-load.service';
import { ChangeStatusService } from 'src/app/core/components/service/change-status.service';
//import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {
  currentUser!: User;
  formData: Login | undefined;
  aSub: any;
  errorMessage!:string;

  constructor(
    private  authService:  AuthService,
     private router: Router,
     private canLogin: CanLoadService,
    private changeStatusService: ChangeStatusService
  ) { }

 public profileForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(4), 
      Validators.pattern('^[a-z0-9_-]{4,16}$')]),
   password: new FormControl('', [Validators.required, Validators.minLength(6),
    Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})')])
  });
  get username() { return this.profileForm.get('username'); }
  get password() { return this.profileForm.get('password'); }
  
  onSubmit() {
    this.formData = {
      username: this.profileForm.value.username,
      password: this.profileForm.value.password
    }
    if(this.profileForm.get('username')?.valid && this.profileForm.get('password')?.valid) {
    this.authService.login(this.formData)
    .subscribe((res: Token) => {
    this.canLogin.show();
    this.changeStatusService.changeStatusFalse();
     this.router.navigate(['/dashboard']);
    });
  }
}
}  

   

