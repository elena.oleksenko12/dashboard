import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthComponent } from './auth.component';
import { usersMock } from './users-mock';
import { of } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;
  //const user = usersMock[0];
  let subject: AuthComponent;

  const SignupService=
  {
  isUsernameTaken() {
    return of(false);
  },
  isPasswordTaken() {
    return of(false);
  },
  signup() {
    return of({ success: true });
  },
};
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthComponent ],
      imports: [
        FormsModule, ReactiveFormsModule, HttpClientTestingModule
      ],
        providers: [HttpClientTestingModule, HttpTestingController,
          { useValue: SignupService }
      
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    expect(component.onSubmit()).toBeFalsy();
  });
});
