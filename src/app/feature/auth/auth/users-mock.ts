import { Registration, Token, User } from "../models";

export const usersMock:  User[] = [
    {
      _id:'1',
      password: '123456aA',
      username: 'jonny1',
      createdDate: '05.11.2022',
      typeColumnTodo: 'todo',
      typeColumnProgress: 'progress',
      typeColumnDone: 'done',
      listColorTodo: 'white',
      listColorProgress: 'pink',
      listColorDone: 'white'
    },
    {
        _id:'2',
        password: '123456bB',
        username: 'jonny2',
        createdDate: '06.11.2022',
        typeColumnTodo: 'todo',
        typeColumnProgress: 'progress',
        typeColumnDone: 'done',
        listColorTodo: 'white',
        listColorProgress: 'pink',
        listColorDone: 'white'
    },
    {
        _id:'3',
        password: '123456cC',
        username: 'jonny3',
        createdDate: '07.11.2022',
        typeColumnTodo: 'todo',
        typeColumnProgress: 'progress',
        typeColumnDone: 'done',
        listColorTodo: 'white',
        listColorProgress: 'pink',
        listColorDone: 'white'
    },
    {
        _id:'4',
        password: '123456dD',
        username: 'jonny4',
        createdDate: '08.11.2022',
        typeColumnTodo: 'todo',
        typeColumnProgress: 'progress',
        typeColumnDone: 'done',
        listColorTodo: 'white',
        listColorProgress: 'pink',
        listColorDone: 'white'
    }
  ]

  export const tokenMock : Token = {
    jwt_token: '112233gyjgj',
    user: {
      _id:'1',
      password: '123456aA',
      username: 'helen',
      createdDate: '02.11.2022',
      //listColor: string,
      typeColumnTodo: 'todo',
        typeColumnProgress: 'progress',
        typeColumnDone: 'done',
        listColorTodo: 'white',
        listColorProgress: 'pink',
        listColorDone: 'white'
    }
}

export const registrationMock : Registration = {
    password: '123456aA',
    username: 'helen'
  }
  
  
  