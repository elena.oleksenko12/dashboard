import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChangeStatusService } from 'src/app/core/components/service/change-status.service';
import { CanLoadService } from 'src/app/services/can-load.service';
import { Registration, User } from '../models';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  currentUser!: User;
  formData: Registration | undefined;
  aSub: any;
  errorMessage!:string;
  statusGood:any;
  bedStatus:boolean=false;
  changeStatus!:any;
  query: string = '';
  queryPas: string = '';

  constructor(
    private  authService:  AuthService,
     private router: Router,
     private canLogin: CanLoadService,
    private changeStatusService: ChangeStatusService,
  ) {}

 public registerForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(4),
    Validators.pattern('^[a-z0-9_-]{4,16}$')]),
   password: new FormControl('', [Validators.required,  Validators.minLength(8),
    Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})')
  ])
  });
 get username() { return this.registerForm.get('username'); }
 get password() { return this.registerForm.get('password'); }
  
  onSubmit() {
    this.formData = {
      username: this.registerForm.value.username,
      password: this.registerForm.value.password
    }
  if(this.registerForm.get('username')?.valid && this.registerForm.get('password')?.valid) {
    this.authService.registration(this.formData)
    .subscribe((res) => {
  this.statusGood=res.message;
    },
    );
     
  setTimeout(() => {
    this.query = '';
    this.queryPas = '';
    this.statusGood=false;
    
  }, 2000);
    } else {
      this.bedStatus=true;
      setTimeout(() => {
        this.bedStatus=false;
      }, 3000);
    }
}
  }
 

