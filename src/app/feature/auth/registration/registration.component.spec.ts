import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RegistrationComponent } from './registration.component';
import { dashboardsMockAll } from 'src/app/mocks/dashboard-mocks';
import { usersMock } from '../auth/users-mock';
import { FormControl, FormGroup } from '@angular/forms';
import { of } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;
  const user = usersMock[0];

  const SignupService=
  {
  isUsernameTaken() {
    return of(false);
  },
  isPasswordTaken() {
    return of(false);
  },
  signup() {
    return of({ success: true });
  },
};
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationComponent ],
      imports: [HttpClientTestingModule],
      providers: [
        { useValue: SignupService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should onSubmit', () => {
    expect(component.onSubmit()).toBeFalsy();
   });
});


