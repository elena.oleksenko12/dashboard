import { Pipe, PipeTransform } from '@angular/core';
import { TaskCurrent } from '../modelsBoard';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any, search: string): any {
    if (items!==undefined) {
    if (search.length === 0) return items;
    return items.filter((item:any)=>item.name.includes(search))
  }
}
}
