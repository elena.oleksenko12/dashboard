import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(locItems: any, args: any[]): any {
   const sortField = args[0];
   let hiLo=true;
   if (args[1] === 'desc') {
     hiLo=false; 
   } 
   if (locItems!==undefined) {
   locItems.sort((a: any, b: any) => {
     if (a[sortField] < b[sortField]) {
       if (hiLo) return -1; else return 1;
     } else if (a[sortField] > b[sortField]) {
       if (hiLo) return 1; else return -1;
     } else {
       return 0;
     }
   }
   );
  }
   return locItems;
   }
}
