import { tasksMock } from '../services/tasks-mock';
import { FilterPipe } from './filter.pipe';

describe('FilterPipe', () => {
  it('create an instance', () => {
    const pipe = new FilterPipe();
    expect(pipe).toBeTruthy();
  });

  describe('filter by name', () => {
    let pipe:FilterPipe;
    beforeEach(() => {
      pipe = new FilterPipe();
    });
  it('should return item which contains "good" in name', () => {
    let arr:[]=[];
    let tempTasksMock = [...arr, ...tasksMock.todo];
    const query='samantha';
    let exp=[tempTasksMock[0]];
    let res=pipe.transform(tempTasksMock, query);
    expect(res).toEqual(exp)
  });
  it('should return origin array in case of empty query', () => {
    let arr:[]=[];
    let tempDashboardsMock = [...arr, ...tasksMock.todo];
    const query = '';
    expect(pipe.transform(tempDashboardsMock, query)).toEqual(tempDashboardsMock);
  });
});
});
