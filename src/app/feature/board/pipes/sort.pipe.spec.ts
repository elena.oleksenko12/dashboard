import { tasksMock } from '../services/tasks-mock';
import { SortPipe } from './sort.pipe';

describe('SortPipe', () => {
  it('create an instance', () => {
    const pipe = new SortPipe();
    expect(pipe).toBeTruthy();
  });

  describe('sort by name', () => {
    let pipe:SortPipe;
    beforeEach(() => {
      pipe = new SortPipe();
    });
    it('should return sorted elements by "name" ascending', () => {
      const query='name';
      let asc='asc';
      let arr:[]=[];
      let tempTasksMock = [...arr, ...tasksMock.todo];
      let exp=[tempTasksMock[0]];
      expect(pipe.transform(tempTasksMock, [query, asc])).toEqual(exp);
    });
    it('should return sorted elements by "name" descending', () => {
      const query='name';
      let asc='desc';
      let arr:[]=[];
      let tempTasksMock = [...arr, ...tasksMock.todo];
      let exp=[tempTasksMock[0]];
      expect(pipe.transform(tempTasksMock, [query, asc])).toEqual(exp);
    });
    it('should return sorted elements by "created date" ascending', () => {
      const query='createdDate';
      let asc='asc';
      let arr:[]=[];
      let tempTasksMock = [...arr, ...tasksMock.todo];
      let exp=[tempTasksMock[0]];
      expect(pipe.transform(tempTasksMock, [query, asc])).toEqual(exp);
    });
    it('should return sorted elements by "created date" descending', () => {
      const query='createdDate';
      let asc='desc';
      let arr:[]=[];
      let tempTasksMock = [...arr, ...tasksMock.todo];
      let exp=[tempTasksMock[0]];
      expect(pipe.transform(tempTasksMock, [query, asc])).toEqual(exp);
    });  
});
});