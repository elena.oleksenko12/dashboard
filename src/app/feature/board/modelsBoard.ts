export interface Tasks {
    tasks: {
        _id: string,
        name: string,
        boardId: string
        userId: string,
        type: string,
        createdDate: string,
        comment: string,
        taskNextId: string,
        taskPrevId: string
}
}
export interface Task {
        _id: string,
        name: string,
        boardId: string,
        userId: string,
        type: string,
        createdDate: string,
        comment: string,
        taskNextId: string,
        taskPrevId: string  
}
export interface TaskCurrent {
    task: {
    _id: string,
    name: string,
    boardId: string,
    userId: string,
    type: string,
    createdDate: string,
    comment: string,
    taskNextId: string,
    taskPrevId: string  

}
}
export interface AllTasks {
    todo: [{
    _id: string,
    name: string,
    boardId: string,
    userId: string,
    type: string,
    createdDate: string,
    comment: string,
    taskNextId: string,
    taskPrevId: string 
}],
progress: [{
    _id: string,
    name: string,
    boardId: string,
    userId: string,
    type: string,
    createdDate: string,
    comment: string,
    taskNextId: string,
    taskPrevId: string  
}],
done: [{
    _id: string,
    name: string,
    boardId: string,
    userId: string,
    type: string,
    createdDate: string,
    comment: string,
    taskNextId: string,
    taskPrevId: string  
}],
}
export interface Comment {
    comment: {
    _id: string,
    body: string,
    taskId: string,
    boardId: string,
    userId: string,
    createdDate: string,
    }
}
export interface currentComment {
    _id: string,
    body: string,
    taskId: string,
    boardId: string,
    userId: string,
    createdDate: string, 
}
export interface AllComments {
    comments: [currentComment]
}

