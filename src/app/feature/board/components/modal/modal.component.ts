import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { ModalTaskService } from '../../services/modal-task.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent {
  @Input() id!:string;
   taskId!:string;
  @Output() deleteItemTask = new EventEmitter<object>();
  newTask!:object;
  @Output() editItemTask = new EventEmitter<object>();
  private element: any;
  nameTask!:string;
  @Input() visibility!:boolean;

  constructor(
    public modalService: ModalTaskService,
    private httpService: HttpService,
    private renderer: Renderer2,
    private el: ElementRef,
  ) {}
  deleteTask() {
    this.taskId=this.modalService.currentId;
    this.httpService.deleteTask(this.id, this.taskId).subscribe((res) => {
    this.deleteItemTask.emit(res);
    });
  }
  updateItemTask(newUpdateTask: object) {
    this.newTask=newUpdateTask;
    this.editItemTask.emit(this.newTask);
    this.closeModalTask(); 
  }
  closeModalTask() {
    this.element = this.el.nativeElement;   
    this.renderer.removeClass(this.element, 'active');
    this.modalService.activeItem = {} ;
  }
  openModalComments() {
    this.modalService.isModalOpenComments=true;
    this.closeModalTask();
  }
  openModalUpdateTask() {
    this.modalService.isModalOpenUpdate=true;
    this.closeModalTask();
  }
}
