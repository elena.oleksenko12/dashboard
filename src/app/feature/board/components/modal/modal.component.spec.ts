import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ModalComponent } from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalComponent ],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should return update task', () => {
    const task= {
      _id: '1',
    name: 'samantha',
    boardId: '1',
    userId: '1',
    type: 'Todo',
    createdDate: '02.11.2022',
    comment: 'something',
    taskNextId: '2',
    taskPrevId: '1'
    }
     expect(component.updateItemTask(task)).toBeFalsy();
   });
  it('should open modal comments', () => {
    expect(component.openModalComments()).toBeFalsy();
   });
   it('should open modal update task', () => {
    expect(component.openModalUpdateTask()).toBeFalsy();
   });
   it('should delete task', () => {
    expect(component.deleteTask()).toBeFalsy();
   });
});
