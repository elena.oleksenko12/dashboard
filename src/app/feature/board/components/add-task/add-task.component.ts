import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalTaskService } from '../../services/modal-task.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddTaskComponent{
  formData!:any;
@Input() id!:string;
@Output() newItemTask = new EventEmitter<object>();

  constructor(
    public modalTaskService: ModalTaskService,
    private httpService: HttpService
  ) { }

  public addTaskForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(4),
      Validators.pattern('^[a-z0-9_-]{4,16}$')]),
    type: new FormControl('', [Validators.required, Validators.minLength(4)]),
  });

  get name() { return this.addTaskForm.get('name'); }

  onSubmit() {
    this.formData = {
      name: this.addTaskForm.value.name,
      type: this.modalTaskService.type,
    }
  
 if(this.addTaskForm.get('name')?.valid) {
  this.httpService.addTask(this.formData, this.id).subscribe(res=> {
    this.newItemTask.emit(res);
    this.modalTaskService.closeModalTask();
    });
  }
}

}
