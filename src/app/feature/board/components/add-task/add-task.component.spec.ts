import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AddTaskComponent } from './add-task.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AddTaskComponent', () => {
  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTaskComponent ],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should mark name as invalid when it has value', () => {
    const name= component.addTaskForm.get('name');
    name?.setValue('helen');
    expect(name?.valid).toBeTruthy();
    fixture.detectChanges();
  });
  it('should mark name as invalid when it has no value', () => {
    const name= component.addTaskForm.get('name');
    name?.setValue(null);
    expect(name?.invalid).toBeTruthy();
    fixture.detectChanges();
  });
  it('should mark name as invalid when its value is less than 4', () => {
    const name= component.addTaskForm.get('name');
    name?.setValue('hi');
    expect(name?.invalid).toBeTruthy();
    fixture.detectChanges();
  });

});
