import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EditComponent } from './edit.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { of } from 'rxjs';

describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;

  beforeEach(async () => {
    

 
    await TestBed.configureTestingModule({
      declarations: [ EditComponent ],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should mark name as invalid when it has value', () => {
    const name= component.editTaskForm.get('name');
    name?.setValue('helen');
    expect(name?.valid).toBeTruthy();
    fixture.detectChanges();
  });
  it('should mark name as invalid when it has no value', () => {
    const name= component.editTaskForm.get('name');
    name?.setValue(null);
    expect(name?.invalid).toBeTruthy();
    fixture.detectChanges();
  });
  it('should mark name as invalid when its value is less than 4', () => {
    const name= component.editTaskForm.get('name');
    name?.setValue('hi');
    expect(name?.invalid).toBeTruthy();
    fixture.detectChanges();
  });
  it('The component renders an element using an input', () => {
    component.id = '1';
    expect(component.id).toBe('1');   
  });
  it('The component renders an element using an input', () => {
    expect(component.onSubmitTask()).toBeFalsy();
  });

  it('The component renders an element using an input', () => {
    let task = {
      task: {
      _id: '1',
      name: 'samantha',
      boardId: '1',
      userId: '1',
      type: 'Todo',
      createdDate: '02.11.2022',
      comment: 'something',
      taskNextId: '2',
      taskPrevId: '1'
  },
}
    const editServiceSpy =jasmine.createSpyObj<HttpService>(['editTasks']);
    editServiceSpy.editTasks.and.returnValue(of(task));
    let name='helen';
    let id='1';
    let ID='1';
  
    editServiceSpy.editTasks({name:name}, id, ID).subscribe((res)=> {
      expect(res).toEqual(task);
    });
  });


});
