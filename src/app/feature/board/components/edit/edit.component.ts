import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http.service';
import { ModalTaskService } from '../../services/modal-task.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditComponent {
 @Input() id!:string;
 taskId!:string;
 @Output() newUpdateTask = new EventEmitter<object>();
tasks!:any;
toDo!:Task[];
inProgress!:Task[];
done!:Task[];
updateTask:any;
  constructor(
    public modalTaskService: ModalTaskService,
    private httpService: HttpService
  ) { }
  public editTaskForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(4),
      Validators.pattern('^[a-z0-9_-]{4,16}$')]),
  });
  get name() { return this.editTaskForm.get('name'); }
  onSubmitTask() {
    this.taskId=this.modalTaskService.currentId;
    if(this.editTaskForm.get('name')?.valid) {
    this.httpService.editTasks({name: this.editTaskForm.value.name},
    this.id, this.taskId).subscribe((res) => {
      res=this.updateTask;
      this.newUpdateTask.emit(res);
      this.modalTaskService.closeModalUpdateTask(res);
    });
  }
}
}
