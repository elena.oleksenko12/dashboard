import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Comment, currentComment  } from '../../modelsBoard';
import { ModalTaskService } from '../../services/modal-task.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit {
  @Input() id!:string;
  @Input() task!:string;
  @Input() idTaskForComments!:string;
  commentCurrent!:currentComment;
  comments!: currentComment[];

  constructor(
    private httpService: HttpService,
    public taskService: ModalTaskService,
  ) { }

  ngOnInit(): void {
    this.httpService.getComments(this.id, this.taskService.idCurComment).subscribe((res) => {
      this.comments=res.comments;
    });
  }
  close() {
    this.taskService.visibility= !this.taskService.visibility;
    this.taskService.isModalShowCommentsComponent=
    !this.taskService.isModalShowCommentsComponent;
  }
  deleteComment(comment:currentComment) {
    this.commentCurrent=comment;
    this.httpService.deleteComment( this.commentCurrent.boardId, this.commentCurrent.taskId,
    this.commentCurrent._id).subscribe((res) => {
this.ngOnInit();
});
  }
}
