import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommentsComponent } from './comments.component';
import { commentsMock } from './comments-mock';
import { of } from 'rxjs';
import { HttpService } from '../../services/http.service';

describe('CommentsComponent', () => {
  let component: CommentsComponent;
  let fixture: ComponentFixture<CommentsComponent>;

  beforeEach(async () => {
    const commentServiceSpy =jasmine.createSpyObj<HttpService>(['getComments']);
    commentServiceSpy.getComments.and.returnValue(of(commentsMock));
    await TestBed.configureTestingModule({
      declarations: [ CommentsComponent ],
      imports: [HttpClientTestingModule],
      providers: [{provide: HttpService, useValue: commentServiceSpy}]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have comments', () => {
    expect(component.comments.length).toBe(1);
  });
  it('should return type task111', (() => {
    expect(component.close()).toBeFalsy();
      }));
});
