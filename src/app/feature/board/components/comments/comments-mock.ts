import { AllComments } from "../../modelsBoard";

export const commentsMock:  AllComments = {
    comments: [
{
    _id: '1',
    body: 'something',
    taskId: '1',
    boardId: '1',
    userId: '1',
    createdDate: '01.11.2022', 
}
]
}