import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TextareaComponent } from './textarea.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('TextareaComponent', () => {
  let component: TextareaComponent;
  let fixture: ComponentFixture<TextareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextareaComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule,
        FormsModule ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(TextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('The component renders an element using an input', () => {
    component.id = '1';
    expect(component.id).toBe('1');
  });
  it('should close Textarea', () => {
    expect(component.closeTextarea()).toBeFalsy();
   });
   it('should close Textarea', () => {
    expect(component.onSubmitComment()).toBeFalsy();
   });
});
