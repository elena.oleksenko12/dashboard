import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalTaskService } from '../../services/modal-task.service';
import { Comment } from '../../modelsBoard';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextareaComponent implements OnInit {
  @Input() boardId!:string;
  @Input() taskId!:string;
  @Input() hasCancelButton:boolean = false;
  @Input() initialText:string='';
  @Output() handleSubmit = new EventEmitter<object>();
  comments: Comment[]=[];
  textBody!:object;
  @Input() id!:string;
  form!: FormGroup;

  constructor(
    private httpService: HttpService,
    public modalTaskService: ModalTaskService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      body: [this.initialText, [Validators.required, Validators.pattern('^[a-z0-9_-]{4,16}$')]]

    });
  }
  get body() { return this.form.get('body'); }
  onSubmitComment() {
  this.textBody=this.form.value; 
  this.taskId = this.modalTaskService.currentId;
  if(this.form.get('body')?.valid) {
  this.httpService.addComment(this.textBody, this.id, this.taskId).
  subscribe((createdComment) => {
    this.handleSubmit.emit(createdComment.comment);
});
this.closeTextarea();
  }
  }
  closeTextarea() {
    this.modalTaskService.isModalOpenComments=false;
  }
}
