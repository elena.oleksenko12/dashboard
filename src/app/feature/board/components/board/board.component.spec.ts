import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BoardComponent } from './board.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SortPipe } from '../../pipes/sort.pipe';
import { FilterPipe } from '../../pipes/filter.pipe';
import { HttpService } from '../../services/http.service';
import { of } from 'rxjs';
import { tasksMock } from '../../services/tasks-mock';
import { ActivatedRoute } from '@angular/router';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;

  beforeEach(async () => {
    const boardServiceSpy =jasmine.createSpyObj<HttpService>(['getTasks']);
    let response = {'message': 'Task UPDATED successfully'};
    boardServiceSpy.getTasks.and.returnValue(of(tasksMock));
  
    await TestBed.configureTestingModule({
      declarations: [ BoardComponent, SortPipe, FilterPipe ],
      imports:[HttpClientTestingModule, RouterTestingModule, ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{provide: HttpService, useValue: boardServiceSpy},
        {provide: ActivatedRoute,
        useValue: {
          params: of({
            id: '2'
          }),
        }
      }
      ]
     
    })
    .compileComponents();
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();  
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have tasks done', () => {
    expect(component.done.length).toBe(1);
  });
  it('should have route id', () => {
    expect(component.id).toBe('2')
  });
  it('should return deleteTask', (() => {
    let task = {
      _id: '1',
      name: 'samantha',
      boardId: '1',
      userId: '1',
      type: 'Todo',
      createdDate: '02.11.2022',
      comment: 'something',
      taskNextId: '2',
      taskPrevId: '1'
    }
component.deleteItemTask(task);
   component.ngOnInit();
  }));
  it('should return addTask', (() => {
    let task = {
      _id: '1',
      name: 'samantha',
      boardId: '1',
      userId: '1',
      type: 'Todo',
      createdDate: '02.11.2022',
      comment: 'something',
      taskNextId: '2',
      taskPrevId: '1'
    }
component.addItemTask(task);
   component.ngOnInit();
  }));
  it('should return updateTask', (() => {
    let task = {
      _id: '1',
      name: 'samantha',
      boardId: '1',
      userId: '1',
      type: 'Todo',
      createdDate: '02.11.2022',
      comment: 'something',
      taskNextId: '2',
      taskPrevId: '1'
    }
component.updateItemTask(task);
   component.ngOnInit();
  }));
  it('should return comment new', (() => {
    let text=  {
      _id: '1',
    body: 'string',
    taskId: '1',
    boardId: '1',
    userId: '1',
    createdDate: '01.11.2022', 
    }
component.createComments(text);
  }));
  it('should return addComment', (() => {
    let text=  'string';
expect(component.addComment(text)).toBeFalsy(); 
  }));
  it('should return "sequenceTasksPrevNext"', (() => {
    let arr = [
      {
        _id: '1',
        name: 'samantha',
        boardId: '1',
        userId: '1',
        type: 'Todo',
        createdDate: '02.11.2022',
        comment: 'something',
        taskNextId: '2',
        taskPrevId: ''
    },
{
        _id: '3',
        name: 'samantha2',
        boardId: '1',
        userId: '1',
        type: 'Todo',
        createdDate: '03.11.2022',
        comment: 'something',
        taskNextId: '',
        taskPrevId: '2'
    },
{
    _id: '2',
    name: 'samantha3',
    boardId: '1',
    userId: '1',
    type: 'Todo',
    createdDate: '04.11.2022',
    comment: 'something',
    taskNextId: '3',
    taskPrevId: '1'
}]
let arr1 = [
  {
    _id: '1',
    name: 'samantha',
    boardId: '1',
    userId: '1',
    type: 'Todo',
    createdDate: '02.11.2022',
    comment: 'something',
    taskNextId: '2',
    taskPrevId: ''
},
{
  _id: '2',
  name: 'samantha3',
  boardId: '1',
  userId: '1',
  type: 'Todo',
  createdDate: '04.11.2022',
  comment: 'something',
  taskNextId: '3',
  taskPrevId: '1'
  
  },
{
    _id: '3',
    name: 'samantha2',
    boardId: '1',
    userId: '1',
    type: 'Todo',
    createdDate: '03.11.2022',
    comment: 'something',
    taskNextId: '',
    taskPrevId: '2'
},
]
expect(component.sequenceTasksPrevNext(arr)).toEqual(arr1); 
  }));

  it('should return type task', (() => {
expect(component.defineTypeTask([])).toBeFalsy();
  }));
});
