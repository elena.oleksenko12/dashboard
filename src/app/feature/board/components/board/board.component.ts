import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { Tasks, Task, TaskCurrent, Comment } from '../../modelsBoard';
import { HttpService } from '../../services/http.service';
import { ActivatedRoute } from '@angular/router'
import { ModalTaskService } from '../../services/modal-task.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  input: string = "";
  color = 'white';
  color2 = 'white';
  color3 = 'white';
  formData!:any;
  SortDirection!:string;
  SortbyParam:string="";
  archiveTasks:any;
  sub:any;
  id:any;
  toDo!:Task[];
  inProgress!:Task[];
  done!:Task[];  
  sData: any;
  tData: any;
  number!:number;;
  numberSecond!:number;
  tasks!:[];
  taskId!:string;
  idTaskForComments!:string;
  list1:any; 
  list2:any;
  list3:any;
  nameTypeToDo:string='ToDo';
  nameTypeProgress:string='In Progress';
  nameTypeDone:string='Done';

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    public modalTaskService: ModalTaskService,
    private el: ElementRef,     
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.httpService.getTasks(this.id).subscribe((result) => {
        if(this.toDo===undefined || this.inProgress===undefined || this.done===undefined 
          || this.toDo.length===0 || this.inProgress.length===0  || this.done.length===0 
          || result.todo===undefined || result.progress===undefined || result.done===undefined) {
        this.toDo=[];
        this.inProgress=[];
        this.done=[];
        }
        if (result.todo!==undefined) {this.toDo=this.sequenceTasksPrevNext(result.todo);}
        if (result.progress!==undefined) {this.inProgress=this.sequenceTasksPrevNext(result.progress);}
        if (result.done!==undefined) {this.done=this.sequenceTasksPrevNext(result.done);}
        });
      });
      let locColorTodo = localStorage.getItem('listColorTodo');
      let locColumnTodo = localStorage.getItem('typeColumnTodo');
      let locColorProgress = localStorage.getItem('listColorProgress');
      let locColumnProgress = localStorage.getItem('typeColumnProgress');
      let locColorDone = localStorage.getItem('listColorDone');
      let locColumnDone = localStorage.getItem('typeColumnDone');
      let el1 = document.querySelector<HTMLElement>(".list-container-1");
      let el2 = document.querySelector<HTMLElement>(".list-container-2");
      let el3 = document.querySelector<HTMLElement>(".list-container-3");
      if(locColorTodo!==null && locColumnTodo && el1!==null) {
        el1.style.background = locColorTodo;
       }
       if(locColorProgress!==null && locColumnProgress && el2!==null) {
        el2.style.background = locColorProgress;
       }
       if(locColorDone!==null && locColumnDone && el3!==null) {
        el3.style.background = locColorDone;
       }
 }
 
sequenceTasksPrevNext(array:Task[]){
   let result:Task[]=[];
  if (array.length>1){
     let map = new Map();
     array.forEach((el)=>{
      map.set(el._id, el);
      if (el.taskPrevId===''){
        result.push(el);      
      }
     }); 
     while (result[result.length-1].taskNextId!==''){
       result.push(map.get(result[result.length-1].taskNextId))
     }
  } else{
    return array;
  }
  return result;
}

addItemTask(newItemTask:any) {
this.ngOnInit();
}
deleteItemTask(deleteItemTask:any) {
  this.ngOnInit();
}
updateItemTask(editItemTask:any) {
  this.ngOnInit();
}
createComments(task:object) {
}
addComment(text:string):void {
}
archiveTask(task:any) {
  let index = this.done.findIndex(function(item:any) {
    return item._id === task._id;
  });
     this.done.splice(index, 1);
     this.httpService.archiveTasks({task:task}, this.id, task._id).subscribe((res)=> {
      this.archiveTasks=res;
});
}
dragStartDriverSch(event:any, data:any) {
    event.dataTransfer.setData("Todo", JSON.stringify(data));
}
dragStartTrailer(event:any, data:any) {
  event.dataTransfer.setData("In Progress", JSON.stringify(data));
}
dragStartDone(event:any, data:any) {
  event.dataTransfer.setData("Done", JSON.stringify(data));
}
dragAndDropAll(event:any, name: string, data:any, toArray:Task[], fromArray:Task[] ){
    if (event.dataTransfer.getData(name)) {
      let schData1:Task = JSON.parse(event.dataTransfer.getData(name));
      if (name===schData1.type){
        let sendType = this.defineTypeTask(toArray);
        if ((data===null)&&(toArray.find((d:any) => d.name === schData1.name) === undefined)) {
          if (sendType!==undefined) schData1.type=sendType;
          toArray.push(schData1);
          this.requestAndDel(fromArray,schData1,schData1.type);
        }
        else {
          if ((data!==null)&&(toArray.find((d:any) => d.name === data.name) !== undefined)){
            let i=0;
            toArray.forEach((element:any) => {
              if(element.name===data.name) {
                this.number=i;
            } else {
                i++;
            }
            return this.number;   
        });
        if (sendType!==undefined) {schData1.type=sendType;}
        toArray.splice(this.number, 0, schData1);
        this.requestAndDel(fromArray,schData1,data._id);
      }  
    }   
  }
}
  }
requestAndDel(fromArray:Task[], schData1:Task, sendType: string){
  let index = fromArray.findIndex(function(item:any) {
          return item.name === schData1.name;
      });
      if (index > -1) {
        fromArray.splice(index, 1);
      }
      this.httpService.updateStatusTasks({taskMoveId: schData1._id,
        inWhoseInserted:sendType},this.id, schData1._id).subscribe((res) => {
        });
}
defineTypeTask(array:Task[]){
  if (array===this.toDo)  return "Todo";
  if (array===this.inProgress)  return "In Progress";
  if (array===this.done)  return "Done";
  return;
}
//1-2
onDropFromTodoToProgress(event:any, array:any) {
  this.dragAndDropAll(event, "Todo",null, this.inProgress, this.toDo);
}
//2-1
onDropFromProgressToTodo(event:any, array:any) {
  this.dragAndDropAll(event, "In Progress",null, this.toDo, this.inProgress);
}
// 1-3
onDropFromTodoToDone(event:any, array:any) {
  this.dragAndDropAll(event, "Todo", null, this.done, this.toDo);
}
//2-3
onDropFromProgressToDone(event:any, array:any) {
  this.dragAndDropAll(event, "In Progress",null,this.done, this.inProgress);
}
//3-2
onDropFromDoneToProgress(event:any, array:any) {
  this.dragAndDropAll(event, "Done",null, this.inProgress, this.done);
}
// 3-1
onDropFromDoneToTodo(event:any, array:any) {
  this.dragAndDropAll(event, "Done",null, this.toDo, this.done);
}

//2-1
onDropFromProgressToTodoOnTask(event:any, data:any) {
  this.dragAndDropAll(event,"In Progress",data, this.toDo, this.inProgress );
}
//1-2
onDropFromTodoToProgressOnTask(event:any, data:any) {
  this.dragAndDropAll(event,"Todo",data, this.inProgress, this.toDo);
}
//3-2
onDropFromDoneToProgressOnTask(event:any, data:any) { 
  this.dragAndDropAll(event,"Done",data, this.inProgress, this.done);
}
//2-3
onDropFromProgressToDoneOnTask(event:any, data:any) {
  this.dragAndDropAll(event,"In Progress",data,  this.done, this.inProgress);
}
//1-3
onDropFromTodoToDoneOnTask(event:any, data:any) {
  this.dragAndDropAll(event, "Todo",data, this.done, this.toDo);
}
//3-1
onDropFromDoneToTodoOnTask(event:any, data:any) {
  this.dragAndDropAll(event,"Done", data, this.toDo, this.done);
}
onDragOver(event:any) {
  event.stopPropagation();
  event.preventDefault();
}
onDragLeave(event:any) {
  event.stopPropagation();
  event.preventDefault();
}
}
