import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ArhiveComponent } from './arhive.component';
import { HttpService } from '../../services/http.service';
import { of } from 'rxjs';
import { archivesMock } from './archives-tasks-mock';

describe('ArhiveComponent', () => {
  let component: ArhiveComponent;
  let fixture: ComponentFixture<ArhiveComponent>;

  beforeEach(async () => {
    const archiveServiceSpy =jasmine.createSpyObj<HttpService>(['getArchiveTasks']);
    let arr = [...[archivesMock]]
    archiveServiceSpy.getArchiveTasks.and.returnValue(of(arr));
    
    await TestBed.configureTestingModule({
      declarations: [ ArhiveComponent ],
       imports: [HttpClientTestingModule],
       providers: [{provide: HttpService, useValue: archiveServiceSpy}]
    })
    .compileComponents();
    fixture = TestBed.createComponent(ArhiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return "ShowMore"', (() => {
    let task = {
      _id: '1',
      name: 'samantha',
      boardId: '1',
      userId: '1',
      type: 'Todo',
      createdDate: '02.11.2022',
      comment: 'something',
      taskNextId: '2',
      taskPrevId: '1'
    }
    let details=!true;
component.ShowMore(task);
component.details=details; 
  }));
  it('The component renders an element using an input', () => {
    component.archives = {
      _id: '3',
      name: 'samantha3',
      boardId: '1',
      userId: '1',
      type: 'Done',
      createdDate: '04.11.2022',
      comment: 'something',
      taskNextId: '4',
      taskPrevId: '3'
    }
    expect(component.archives.name).toBe('samantha3');
  });
  it('should close', () => {
    expect(component.close()).toBeFalsy();
  });
});
