import { Tasks } from "../../modelsBoard";

export const archivesMock:  Tasks = {
    tasks: {
        _id: 'id',
        name: 'name',
        boardId: '1',
        userId: '1',
        type: 'In Progress',
        createdDate: '02.11.2022',
        comment: 'comment',
        taskNextId: '2',
        taskPrevId: '1'
}
}