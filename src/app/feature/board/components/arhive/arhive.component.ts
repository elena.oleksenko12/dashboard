import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Tasks, Task } from '../../modelsBoard';
import { HttpService } from '../../services/http.service';
import { ModalTaskService } from '../../services/modal-task.service';

@Component({
  selector: 'app-arhive',
  templateUrl: './arhive.component.html',
  styleUrls: ['./arhive.component.scss'],
})
export class ArhiveComponent implements OnInit {
  archives!:any;
  @Input() id!:string;
  details:boolean=false;

  constructor(
    private httpService: HttpService,
    public taskService: ModalTaskService
  ) { }

  ngOnInit(): void {
    this.httpService.getArchiveTasks(this.id).subscribe((res) => {
      this.archives=res.tasks;
  });
  }
  ShowMore(archive:Task) {
    this.details = !this.details;
  }
  close() {
   this.taskService.isArchiveOpen = !this.taskService.isArchiveOpen;
  }
}
