import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ColorListButtonComponent } from './color-list-button.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpService } from '../../services/http.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ColorListButtonComponent', () => {
  let component: ColorListButtonComponent;
  let fixture: ComponentFixture<ColorListButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorListButtonComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA], 
    })
    .compileComponents();
    fixture = TestBed.createComponent(ColorListButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should send an event on click', () => {
    const event = spyOn(component.СhoiceByParam, 'emit');
    component.selectedType='Progress';
  
    component.onChange('Progress');
    expect(event).toHaveBeenCalled();
  });

it('should call changeColorList', () => {
  const e = document.createEvent('event');
});


});
