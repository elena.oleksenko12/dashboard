import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-color-list-button',
  templateUrl: './color-list-button.component.html',
  styleUrls: ['./color-list-button.component.scss'],
})
export class ColorListButtonComponent implements OnInit {
  @Input() id!:string;
  @Output() newColor = new EventEmitter<string>();
  list1:any;
  list2:any;
  list3:any;
  colorWell!:any;
  defaultColor:string = '#ffffff';
  window!:any;
  boardId!:string;
  @Input() selectedType: string = "";
  @Output() СhoiceByParam = new EventEmitter<any>();

  constructor(private httpService: HttpService,
    private renderer: Renderer2,
    private route: ActivatedRoute,) { 
    }

  ngOnInit(): void {
      this.route.params.subscribe(params => {
        this.boardId = params['id'];
       });
      this.colorWell = document.querySelector(".colorWell");
      this.colorWell.value = this.defaultColor;
      this.colorWell.select(); 
  }
  onChange(model:string) {
    this.selectedType = model;
    this.СhoiceByParam.emit(this.selectedType);
  }
updateOne(event:any) {
    let el1 = document.querySelector<HTMLElement>(".list-container-1");
    let el2 = document.querySelector<HTMLElement>(".list-container-2");
    let el3 = document.querySelector<HTMLElement>(".list-container-3");
  
    if (el1 && this.selectedType==='todo') {
      el1.style.background = event.target.value;
    } else if(el2 && this.selectedType==='progress') {
      el2.style.background = event.target.value;
    } else if(el3 && this.selectedType==='done'){
      el3.style.background = event.target.value;
    }
    this.httpService.changeColorList({color:event.target.value, type:this.selectedType}, this.boardId).subscribe((res)=> {
    });
   if(this.selectedType==='todo') {
    localStorage.setItem('typeColumnTodo', this.selectedType);
    localStorage.setItem('listColorTodo', event.target.value);
   } else if(this.selectedType==='progress') {
    localStorage.setItem('typeColumnProgress', this.selectedType);
    localStorage.setItem('listColorProgress', event.target.value);
   } else {
    localStorage.setItem('typeColumnDone', this.selectedType);
    localStorage.setItem('listColorDone', event.target.value);
   }
    
    this.newColor.emit(event.target.value);
  }
  }
    document.querySelectorAll<HTMLElement>(".item").forEach(function(el) {
    el.style.backgroundColor = 'black';
    });
 

  

