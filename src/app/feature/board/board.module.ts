import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './components/board/board.component';
import { BoardRoutingModule } from './board-routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { ModalTaskService } from './services/modal-task.service';
import { HttpService } from './services/http.service';
import { ModalComponent } from './components/modal/modal.component';
import { EditComponent } from './components/edit/edit.component';
import { FilterPipe } from './pipes/filter.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { CommentsComponent } from './components/comments/comments.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { ArhiveComponent } from './components/arhive/arhive.component';
import { FocusDirective } from './directives/focus.directive';
import { ColorListButtonComponent } from './components/color-list-button/color-list-button.component';

@NgModule({
  declarations: [
    BoardComponent,
    AddTaskComponent,
    ModalComponent,
    EditComponent,
    FilterPipe,
    SortPipe,
    CommentsComponent,
    TextareaComponent,
    ArhiveComponent,
    FocusDirective,
    ColorListButtonComponent,
  ],
  imports: [
    CommonModule,
    BoardRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports: [BoardComponent],
  providers: [ModalTaskService, HttpService]
})
export class BoardModule { }
