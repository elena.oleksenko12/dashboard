import { TestBed } from '@angular/core/testing';

import { ModalTaskService } from './modal-task.service';

describe('ModalTaskService', () => {
  let service: ModalTaskService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalTaskService],
    });
    service = TestBed.inject(ModalTaskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should openModalTask', () => {
    let type='Todo';
    expect(service.openModalTask(type)).toBeFalsy();
  });

  it('should getComments', () => {
    let id='1';
    let task= {
      _id: '3',
    name: 'samantha3',
    boardId: '1',
    userId: '1',
    type: 'Done',
    createdDate: '04.11.2022',
    comment: 'something',
    taskNextId: '4',
    taskPrevId: '3'
    }
    expect(service.getComments(id, task)).toBeFalsy();
  });
  it('should showArchiveTasks', () => {
    let type='Todo';
    expect(service.showArchiveTasks()).toBeFalsy();
  });
  it('should  closeModalUpdateTask', () => {
    let task= {
      _id: '3',
    name: 'samantha3',
    boardId: '1',
    userId: '1',
    type: 'Done',
    createdDate: '04.11.2022',
    comment: 'something',
    taskNextId: '4',
    taskPrevId: '3'
    }
    expect(service. closeModalUpdateTask(task)).toBeFalsy();
  });

  it('should closeModal', () => {
    expect(service.closeModal()).toBeFalsy();
  });

  it('should closeModalTask', () => {
    expect(service.closeModalTask()).toBeFalsy();
  });

  it('should closeModalComments', () => {
    let comment= {
      _id: '1',
      body: 'string',
      taskId: '1',
      boardId: '1',
      userId: '1',
      createdDate: '01.11.2022',
    }
    expect(service.closeModalComments(comment)).toBeFalsy();
  });

  it('should open modal', () => {
    spyOn(service, 'openModal').and.callThrough();
let task= {
}
const type = true;
    const expected = 4;
    let id='1';
    service.openModal(id, task);
    let curId;
  })
});


