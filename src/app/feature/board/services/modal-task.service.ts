import { EventEmitter, Injectable, Renderer2 } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ModalTaskService {
  type:any;
  currentId!:string;
  resultsUpdate!:object;
  nameTask!:string;
  idTask!:string;
  currentTaskComment!:object;
  public activeItem!: object;
  idCurComment!:string;
  public activeComment!: object;
  public desactiveButton!: object;
  public closeButton!: object;
  public hiddenButton!: object;
  isModalOpen:boolean=false;
  isModalTaskOpen:boolean=false;
  isModalOpenUpdate:boolean=false;
  isModalOpenComments:boolean=false;
  isModalShowCommentsComponent:boolean=false;
  isArchiveOpen:boolean=false;
  taskArchive!:object;
  public eventEmit: any;
  buttonTitle:string='Close me';
  visibility: boolean = false;
  constructor(
  ) {
    this.eventEmit = new EventEmitter();
  }
openModal(id:string, task:object) {
  this.currentId=id;
  this.activeItem = task;
}
openModalTask(type:any) {
  this.isModalTaskOpen = !this.isModalTaskOpen;
  this.type=type;
}
getComments(id:string, task:object) {
  this.visibility=!this.visibility;
  this.isModalShowCommentsComponent=!this.isModalShowCommentsComponent;
  this.currentTaskComment=task;
  this.idCurComment=id;
  this.activeComment=task;
}
showArchiveTasks() {
  this.isArchiveOpen = !this.isArchiveOpen;
}
closeModalUpdateTask(res:object) {
  this.isModalOpenUpdate=false;
  res=this.resultsUpdate;
}
closeModal() {
  this.isModalOpen = false;
}
closeModalTask() {
  this.isModalTaskOpen = false;
  }
closeModalComments(close:any) {
  close=this.hiddenButton;
  }
}
