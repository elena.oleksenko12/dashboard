import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, take, tap } from 'rxjs';
import { Tasks, Task, TaskCurrent, Comment, currentComment, AllTasks, AllComments } from '../modelsBoard';
@Injectable({
  providedIn: 'root'
})
export class HttpService {
  comments!:currentComment[];
  todo!:Task[];
  progress!:Task[];
  done!:Task[];
  constructor(
    private http: HttpClient
  ) { }
getTasks(id:string): Observable<AllTasks> {
  return this.http.get<AllTasks>(`http://localhost:8080/api/task/${id}`)
  .pipe(
  take(1),
  tap(
    (res: AllTasks) => {
    }));
}
addTask(model: TaskCurrent, id:string): Observable<TaskCurrent> {
  return this.http.post<TaskCurrent>(`http://localhost:8080/api/task/${id}`,  model)
  .pipe(
  take(1),
  tap(
  (res: TaskCurrent) => {
  }));
}
updateStatusTasks(model: any, id:string, taskId:string): Observable<any> {
  return this.http.patch<any>(`http://localhost:8080/api/task/${id}/move/${taskId}`,  model)
  .pipe(
  take(1),
  tap(
  (res: any) => {
}));
}
deleteTask(idBoard: string, idTask:string): Observable<Task> { 
  return this.http.delete<Task>(`http://localhost:8080/api/task/${idBoard}/${idTask}`)
  .pipe(
    take(1),
  tap(
    (res: Task) => {
    }
  ));
}
editTasks(model: object, idBoard: string, idTask:string): Observable<TaskCurrent> {
  return this.http.put<TaskCurrent>(`http://localhost:8080/api/task/${idBoard}/${idTask}`,  model)
  .pipe(
  take(1),
  tap(
  (res: TaskCurrent) => {
}));
}
archiveTasks(model: object, idBoard: string, idTask:string): Observable<TaskCurrent> {
  return this.http.patch<TaskCurrent>(`http://localhost:8080/api/task/${idBoard}/${idTask}`,  model)
  .pipe(
  take(1),
  tap(
  (res: TaskCurrent) => { 
  }));
}
getArchiveTasks(idBoard: string): Observable<any> {
  return this.http.get<any>(`http://localhost:8080/api/task/${idBoard}/archive`)
  .pipe(
  take(1),
  tap(
  (res: any) => {
  }));
}
addComment(model: object, id:string, idTask:string): Observable<Comment> {
  return this.http.post<Comment>(`http://localhost:8080/api/comment/${id}/${idTask}`,  model)
  .pipe(
  take(1),
  tap(
  (res: Comment) => {
  }));
}
getComments(id:string, idTask:string): Observable<AllComments> {
  return this.http.get<AllComments>(`http://localhost:8080/api/comment/${id}/${idTask}`)
  .pipe(
  take(1),
  tap(
  (res: AllComments) => {
  this.comments=res.comments;
  }));
}
deleteComment(idBoard: string, idTask:string, idComment:string): Observable<currentComment> { 
  return this.http.delete<currentComment>(`http://localhost:8080/api/comment/${idBoard}/${idTask}/${idComment}`)
  .pipe(
  take(1),                  
  tap(               
  (res: currentComment) => {                                
  }
  ));
} 
changeColorList(model: object, idBoard: string): Observable<any> {
  return this.http.patch<any>(`http://localhost:8080/api/task/${idBoard}`, model)
  .pipe(
  take(1),
  tap(
  (res: any) => {
}));
}
}




