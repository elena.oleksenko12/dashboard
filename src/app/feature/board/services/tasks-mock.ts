import { AllComments, AllTasks } from "../modelsBoard";

export const tasksMock: AllTasks = {
    todo: [
        {
        _id: '1',
        name: 'samantha',
        boardId: '1',
        userId: '1',
        type: 'Todo',
        createdDate: '02.11.2022',
        comment: 'something',
        taskNextId: '2',
        taskPrevId: '1'
    },
],
progress: [{
        _id: '2',
        name: 'samantha2',
        boardId: '1',
        userId: '1',
        type: 'In Progress',
        createdDate: '03.11.2022',
        comment: 'something',
        taskNextId: '3',
        taskPrevId: '2'
    },
],
done: [{
    _id: '3',
    name: 'samantha3',
    boardId: '1',
    userId: '1',
    type: 'Done',
    createdDate: '04.11.2022',
    comment: 'something',
    taskNextId: '4',
    taskPrevId: '3'

}]
}
export const commentsMock: AllComments = {
    comments: [
        {
            _id: '1',
            body: 'string',
            taskId: '1',
            boardId: '1',
            userId: '1',
            createdDate: '01.11.2022', 
    }
]
}



