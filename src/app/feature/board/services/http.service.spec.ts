import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { HttpService } from './http.service';
import { commentsMock, tasksMock } from './tasks-mock';

describe('HttpService', () => {
  let id='1';
  describe('HttpService (with spies)', () => {
    let httpClientSpy: jasmine.SpyObj<HttpClient>;
    let service: HttpService;
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','put', 'post', 'patch', 'delete']);
    service = new HttpService(httpClientSpy);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should return tasks (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(tasksMock));
    service.getTasks(id).subscribe({
      next: dashboards => {
        expect(dashboards).toEqual(tasksMock);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.get.calls.count()).toBe(1);
  });
  it('should return edit task (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.put.and.returnValue(of(tasksMock.done[0]));
    let id='1';
    let idTask='1';
    let data={
      name: 'afternoon'
    }
    service.editTasks(data,id,idTask).subscribe({
      next: task => {
        expect(task).toBeTruthy();
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.put.calls.count()).toBe(1);
  });
  it('should return delete task (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.delete.and.returnValue(of(tasksMock.progress[0]));
    let id='1';
    let idTask='1';
    service.deleteTask(id,idTask).subscribe({
      next: data => {
        expect(data).toEqual(tasksMock.progress[0]);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1);
  });


  it('should return post task (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(tasksMock.todo[0]));
    const taskNew = {
      _id: '1',
      name: 'samantha',
      boardId: '1',
      userId: '1',
      type: 'Todo',
      createdDate: '02.11.2022',
      comment: 'something',
      taskNextId: '2',
      taskPrevId: '1'
  }
  let id='1';
    service.addTask({task:taskNew}, id).subscribe({
      next: task => {
        expect(task.task).toBeFalsy();
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.post.calls.count()).toBe(1);
  });


  it('should return update status task (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.patch.and.returnValue(of(tasksMock.done[0]));
    let id='1';
    let idTask='1';
    let data={
      type: 'In Progress'
    }
    service.updateStatusTasks(data,id,idTask).subscribe({
      next: task => {
        expect(task).toBeTruthy();
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.patch.calls.count()).toBe(1);
  });
  it('should return comments (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.get.and.returnValue(of(commentsMock));
    let id='1';
    let taskId='1';
    service.getComments(id, taskId).subscribe({
      next: tasks => {
        expect(tasks).toEqual(commentsMock);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.get.calls.count()).toBe(1);
  });
  it('should return delete comment (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.delete.and.returnValue(of(commentsMock.comments[0]));
    let id='1';
    let idTask='1';
    let idComment='1';
    service.deleteComment(id,idTask, idComment).subscribe({
      next: data => {
        expect(data).toEqual(commentsMock.comments[0]);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.delete.calls.count()).toBe(1);
  });
  it('should return post comment (HttpClient called once)', (done: DoneFn) => {
    httpClientSpy.post.and.returnValue(of(commentsMock.comments));
    const data = {
        _id: '1',
            body: 'string',
            taskId: '1',
            boardId: '1',
            userId: '1',
            createdDate: '01.11.2022', 
}
  let id='1';
  let idTask='1';
    service.addComment(data, id, idTask).subscribe({
      next: tasks => {
        expect(tasks.comment).toBeFalsy();
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.post.calls.count()).toBe(1);
  });
});
  });


 
