import { Component } from '@angular/core';
import { ErrorGlobalService } from 'src/app/services/error-global.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent  {
  constructor(public errorService: ErrorGlobalService) { }
}
