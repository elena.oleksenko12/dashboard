import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './feature/auth/auth/auth.component';
import { RegistrationComponent } from './feature/auth/registration/registration.component';
import { AddDashboardComponent } from './feature/dashboard/components/add-dashboard/add-dashboard.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {path: 'login', component: AuthComponent,
  },
  {path: 'registration', component: RegistrationComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'dashboard', 
  loadChildren: ()=> import('./feature/dashboard/dashboard.module').then(m=>m.DashboardModule),
  canActivateChild:[AuthGuard]
  },
  {path: 'board/:id', 
  loadChildren: ()=> import('./feature/board/board.module').then(m=>m.BoardModule),
  canActivateChild:[AuthGuard]
  }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
