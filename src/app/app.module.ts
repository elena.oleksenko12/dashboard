import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {  httpInterceptorProviders } from './interceptors/http-interceptor.interceptor';
import { CoreModule } from './core/core.module';
import { AuthModule } from './feature/auth/auth.module';

import { DashboardModule } from './feature/dashboard/dashboard.module';
import { DashboardService } from './feature/dashboard/services/dashboard.service';
import { BoardRoutingModule } from './feature/board/board-routing';
import { BoardModule } from './feature/board/board.module';
import { AuthGuard } from './services/auth.guard';
import { ErrorComponent } from './feature/error/error.component';
import { ErrorGlobalService } from './services/error-global.service';




@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    AuthModule,
    DashboardModule,
  
    RouterModule,
    FormsModule,
    HttpClientModule,
    BoardRoutingModule,
    BoardModule
    
    

  ],
  providers: [httpInterceptorProviders,
    DashboardService, AuthGuard, ErrorGlobalService],

  bootstrap: [AppComponent]
})
export class AppModule { }
