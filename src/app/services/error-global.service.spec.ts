import { TestBed } from '@angular/core/testing';

import { ErrorGlobalService } from './error-global.service';

describe('ErrorGlobalService', () => {
  let service: ErrorGlobalService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorGlobalService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should clear value', () => {
    expect(service.clear).toBeTruthy();
  });
  it('should change value', (() => {
    service.clear();
    service.error$.subscribe(value => {
      expect(value).toBe('');
});
  }));
});
