import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CanLoadService {

  AUTH_USER_NAME: any;
public isUserLoggedIn$!: Observable<boolean>;
  public isUserLoggedIn$$;
  constructor(
  ) { 
    if(localStorage.getItem('status')==='true') {
      this.isUserLoggedIn$$ = new BehaviorSubject(true);
    } else {
      this.isUserLoggedIn$$ = new BehaviorSubject(false);
    }
    this.isUserLoggedIn$ = this.isUserLoggedIn$$.asObservable();
  }
  public show() {
    return this.isUserLoggedIn$$.next(true);
  }
  public hide() {
    return this.isUserLoggedIn$$.next(false);
  }
  public getState() {
    this.isUserLoggedIn$$.getValue();
  }
}

