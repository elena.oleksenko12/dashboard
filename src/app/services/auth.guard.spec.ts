import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthGuard } from './auth.guard';

class MockRouter {
  navigate(path:any) {}
}

describe('AuthGuard', () => {
  describe('canActivate', () => {
    let authGuard: AuthGuard;
    let router;
  beforeEach(() => {
    authGuard = TestBed.inject(AuthGuard);
    router = TestBed.inject(Router);
    spyOn(router, 'navigate')
  });
  it('should be created', () => {
    expect(authGuard).toBeTruthy();
  });
  it('should canActivate true', () => {
    expect(authGuard.canActivateChild()).toBeTruthy();
  });
});

});