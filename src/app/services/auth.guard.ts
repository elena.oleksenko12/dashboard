import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CanLoadService } from './can-load.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {
  constructor(
    private canLoadService: CanLoadService,
    private router: Router
  ) {
  }
  canActivateChild():boolean {
    if(this.canLoadService.isUserLoggedIn$$) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    } 
  }
}
