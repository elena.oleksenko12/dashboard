import { TestBed } from '@angular/core/testing';
import { CanLoadService } from './can-load.service';

describe('CanLoadService', () => {
  let service: CanLoadService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanLoadService]
    });
    service = TestBed.inject(CanLoadService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should change value "true"', (() => {
    service.show();
    service.isUserLoggedIn$$.subscribe(value => {
      expect(value).toBe(true);
});
  }));
  it('should change value "false"', (() => {
    service.hide();
    service.isUserLoggedIn$$.subscribe(value => {
      expect(value).toBe(false);
});
  }));
});
