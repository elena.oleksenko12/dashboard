import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChangeStatusService } from '../service/change-status.service';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      providers: [ChangeStatusService, HttpClientTestingModule, HttpTestingController],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getObsTrue', () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const app = fixture.componentInstance;
    const service = fixture.debugElement.injector.get(ChangeStatusService);
    service.changeStatusTrue();
    expect(app.canLoad).toBeTruthy();
  });
  it('should call getObsFalse', () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const app = fixture.componentInstance;
    const service = fixture.debugElement.injector.get(ChangeStatusService);
    service.changeStatusFalse();
    expect(app.canLoad).toBeTruthy();
  });
  beforeEach(() => {
    let store:any = {};
    spyOn(localStorage, 'getItem').and.callFake( (key:any):string => {
     return store[key] || null;
    });
    spyOn(localStorage, 'removeItem').and.callFake((key:string):void =>  {
      delete store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake((key:string, value:string):string =>  {
      return store[key] = <string>value;
    });
    spyOn(localStorage, 'clear').and.callFake(() =>  {
        store = {};
    });
  });
it('should set an Item', () => {
  expect(localStorage.setItem('foo', 'bar')).toBeTruthy(); // bar
  expect(localStorage.getItem('foo')).toBe('bar'); // bar
});
});
