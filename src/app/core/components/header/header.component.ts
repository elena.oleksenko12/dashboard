import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { CanLoadService } from 'src/app/services/can-load.service';
import { ChangeStatusService } from '../service/change-status.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {

AUTH_USER_NAME:any;
token:any=localStorage.getItem('token');
status!: boolean;
public isVisiable=false;
statusForButton!:boolean;
localStatucForButton!:boolean;
  constructor(
    public canLoad: CanLoadService,
    private router: Router,
    public changeStatusService: ChangeStatusService,
     ) {}

  ngOnInit(): void {
    this.canLoad.isUserLoggedIn$$.subscribe(value => {
      this.status = value;
      localStorage.setItem('status', JSON.stringify(this.status));
      this.AUTH_USER_NAME=localStorage.getItem('username');
  });
  let localStatucForButton = localStorage.getItem('statusForButton');
  if (localStatucForButton!==undefined && localStatucForButton==="false") {
      this.changeStatusService.changeStatusFalse();
  }
  this.changeStatusService.status$.subscribe(value => {
    this.statusForButton = value;
  }); 
}
  ngOnDestroy(): void {
    this.canLoad.isUserLoggedIn$$.unsubscribe();
  }
  followingLink() {
   if(this.statusForButton) {
    localStorage.setItem('statusForButton', 'false');
    this.changeStatusService.changeStatusFalse();
    this.router.navigate(['/registration']);
   } else {
    localStorage.setItem('statusForButton', 'true');
    this.changeStatusService.changeStatusTrue();
   this.router.navigate(['/login']);
   }
  }  
  logOut() {
    localStorage.clear();
    this.canLoad.hide();
    this.changeStatusService.changeStatusTrue();
    this.router.navigate(['/login']);
}
}
