import { TestBed } from '@angular/core/testing';
import { BehaviorSubject, Observable } from 'rxjs';

import { ChangeStatusService } from './change-status.service';

describe('ChangeStatusService', () => {
  let service: ChangeStatusService;

  beforeEach(() => {
    service = new ChangeStatusService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getValue should return real value', () => {
 
    expect(service.changeStatusTrue()).toBeFalsy();
  });

  it('should return value', (() => {
    service.getChangeStatus()
    service.status$.subscribe(value => {
      expect(value).toBeTruthy();;
});
  }));
});
