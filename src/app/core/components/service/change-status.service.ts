import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangeStatusService {
  private subject = new BehaviorSubject<boolean>(true);
  status$: Observable<boolean> = this.subject.asObservable();

public changeStatusTrue() {
    this.subject.next(true);
  }
  public changeStatusFalse() {
    this.subject.next(false);
}
public getChangeStatus() {
    this.subject.getValue();
}
}
