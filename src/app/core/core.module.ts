import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ChangeStatusService } from './components/service/change-status.service';




@NgModule({
  declarations: [
    HeaderComponent,
  ],
  imports: [
    CommonModule
  ],
  providers: [ChangeStatusService],
  exports: [HeaderComponent]
})
export class CoreModule { }
