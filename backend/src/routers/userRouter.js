const express = require('express');
const router = express.Router();
const { getUserProfile, deleteUser } = require('../controllers/userController');
router.get('/', getUserProfile);
router.delete('/', deleteUser);

module.exports = {
  userRouter: router,
};
