const express = require('express');
const router = express.Router();
const {
  getBoards, createBoards, getBoardById, updateBoardById,
  deleteBoardById
} = require('../controllers/dashboardController');

router.post('/', createBoards);
router.get('/', getBoards);
router.get('/:id', getBoardById);
router.patch('/:id', updateBoardById);
router.delete('/:id', deleteBoardById);

module.exports = {
dashboardRouter: router,
};
