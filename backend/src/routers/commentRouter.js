const express = require('express');
const router = express.Router();
const { addComment, deleteComment, getComments } = require('../controllers/commentController');
router.post('/:id/:id', addComment);
router.get('/:id/:id', getComments);
router.delete('/:id/:id/:id', deleteComment);

module.exports = {
  commentRouter: router,
};
