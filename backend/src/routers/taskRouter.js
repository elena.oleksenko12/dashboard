const express = require('express');
const router = express.Router();
const {
  getTasks, 
  createTasks, 
  changeColorList, 
  updateTaskById,
  archiveTask,
  getArchiveTasks,
  updateStatusTask,
  deleteTaskById
} = require('../controllers/taskController');
router.post('/:id', createTasks);
router.get('/:id', getTasks);
router.patch('/:id', changeColorList);
router.get('/:id/archive', getArchiveTasks);
router.patch('/:id/:id', archiveTask);
router.patch('/:id/move/:id', updateStatusTask);
router.put('/:id/:id', updateTaskById);
router.delete('/:id/:id', deleteTaskById);

module.exports = {
taskRouter: router,
};
