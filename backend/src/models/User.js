const mongoose = require('mongoose');
const Joi = require('joi');

const User = mongoose.model('User', {
username: {
    type: String,
    required: true,
},
  password: {
    type: String,
    required: true,
    unique: true,
  },
  listColorTodo: {
    type: String,
    default: 'white'
  },
  listColorProgress: {
    type: String,
    default: 'white'
  },
  listColorDone: {
    type: String,
    default: 'white'
  },
  typeColumnTodo: {
    type: String,
    default: ''
  },
  typeColumnProgress: {
    type: String,
    default: ''
  },
  typeColumnDone: {
    type: String,
    default: ''
  },
});

module.exports = {
  User,
};
