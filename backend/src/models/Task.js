const mongoose = require('mongoose');
const Joi = require('joi');

const Task = mongoose.model('Task', {
    name: { 
        type: String, 
        required: true
    },
    userId: { 
        type: mongoose.Schema.Types.ObjectId,
        required: true, 
    },
    boardId: { 
        type: mongoose.Schema.Types.ObjectId,
        required: true, 
    },
     type: { 
        type: String,
        enum: ['Todo', 'In Progress', 'Done', 'Archive'],
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now(),
      },
      taskNextId: {
        type: String, 
      },
      taskPrevId: {
        type: String, 
      },
      comment: {
        type: String,
        default: ''
      },
});

module.exports = {
    Task
};



 