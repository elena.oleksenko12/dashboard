const mongoose = require('mongoose');
const Joi = require('joi');

const Dashboard = mongoose.model('Dashboard', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
      todo: {
        type: Number,
        default: 0
      },
      progress: {
        type: Number,
        default: 0
      },
      done: {
        type: Number,
        default: 0
      },   
});

module.exports = {
  Dashboard,
};
