const mongoose = require('mongoose');
const Joi = require('joi');

const Comment = mongoose.model('Comment', {
    body: { 
        type: String, 
        required: true
    },
    userId: { 
        type: mongoose.Schema.Types.ObjectId,
        required: true, 
    },
    boardId: { 
        type: mongoose.Schema.Types.ObjectId,
        required: true, 
    },
    taskId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true, 
    },
    createdDate: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = {
    Comment
};

