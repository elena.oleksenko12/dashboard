const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const PORT = 8080;
require('dotenv').config();
//mongoose.connect('mongodb://localhost:27017/myapp');
mongoose.connect('mongodb+srv://OlenaOleksenko:5227337109531N@olenaoleksenkomongodbcl.jwck9ba.mongodb.net/users?retryWrites=true&w=majority');
//mongoose.connect(process.env.DB_HOST);
const { authRouter } = require('./routers/authRouter');
const { userRouter } = require('./routers/userRouter');
const { dashboardRouter } = require('./routers/dashboardRouter');
const { taskRouter } = require('./routers/taskRouter');
const { commentRouter } = require('./routers/commentRouter');
const { authMiddleware } = require('./middlewares/authMiddleware');
app.use(express.json());
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(cors());
app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/dashboards', authMiddleware, dashboardRouter);
app.use('/api/task', authMiddleware, taskRouter);
app.use('/api/comment', authMiddleware, commentRouter);
const start = async () => {
  try {
    app.listen(PORT, () => {
      console.log(`Server started on ${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};
start();
// ERROR HANDLER
app.use(errorHandler);
function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'My Server error' });
}
