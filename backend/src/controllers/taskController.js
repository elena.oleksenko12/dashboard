const { Dashboard } = require('../models/Dashboard');
const { Task } = require('../models/Task');
const { User } = require('../models/User');

async function createTasks(req, res, next) {
  try {
    const { name, type } = req.body;
const arrayTodo=[];
const arrayProgress=[];
const arrayDone=[];
let todo;
let progress;
let done;
let lastTaskId='';
const tasksCurrent=[];
const boardIdCurrent= req.params.id;
let task;

if (!name || !type) {
      res.status(400).json({ message: 'The field must not be empty' });
    }
    let tasks = await Task.find({ boardId: boardIdCurrent }, '-__v');
    tasks.find((task) => {
      if(task.type === type && task.taskNextId==='') {
        lastTaskId=task._id;
      }
    });
    if (lastTaskId===''){
        task = new Task({
          name,
          type,
          userId: req.user.userId,
          boardId: boardIdCurrent,
          taskNextId: '',
          taskPrevId: ''
        });
        await task.save();
    } else {     
        task = new Task({
          name,
          type,
          userId: req.user.userId,
          boardId: boardIdCurrent,
          taskNextId: '',
          taskPrevId: lastTaskId
        });
        await task.save();       
        const newCreatedTaskId = await Task.findOne({_id: task._id});
        const lastTask = await Task.findByIdAndUpdate(
          { _id: lastTaskId, userId: req.user.userId },
          { $set:  {taskNextId: newCreatedTaskId._id}  }
        );
    }
countsNumberTaskType(boardIdCurrent, req);
   res.status(200).json({
    task
  });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({
      message: 'My Server error',
    });
  }
}

async function countsNumberTaskType(boardIdCurrentLoc, req){
let numberTodo=0;
let numberProgress=0;
let numberDone=0;
  let tasksLoc = await Task.find({ boardId: boardIdCurrentLoc }, '-__v');
    tasksLoc.forEach((el) => {
      if(el.type==='Todo') { numberTodo++;} 
      if(el.type==='In Progress') { numberProgress++;} 
      if(el.type==='Done') { numberDone++;} 
    });
    const dashboardTask= await Dashboard.findByIdAndUpdate(
      { _id: boardIdCurrentLoc, 
        userId: req.user.userId },
      { $set:  {todo:numberTodo,progress:numberProgress,done:numberDone}  }
    );
}

async function getTasks(req, res, next) {
  try {
    const arrayTodo=[];
    const arrayProgress=[];
    const arrayDone=[];
    let todo;
    let progress;
    let done;
    const boardIdCurrent= req.params.id;
    const tasks = await Task.find({ boardId: boardIdCurrent }, '-__v');
    tasks.forEach((el) => {
      if(el.type==='Todo') {
        arrayTodo.push(el);
        todo =arrayTodo;
      }
      if(el.type==='In Progress') {
        arrayProgress.push(el);
        progress = arrayProgress;
      }
      if(el.type==='Done') {
        arrayDone.push(el);
        done = arrayDone;
      }
    });
      res.status(200).json({
       todo,
        progress,
        done
      });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      message: 'My Server error',
    });
  }

}

const updateTaskById = async (req, res, next) => {
  try{
  const { name } = req.body;
   const taskNew= await Task.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { name } },
  );
     const task= await Task.findById({_id: req.params.id});
      res.status(200).json({ 
        task
       });
} catch(error) {
  console.log(error.message)
  res.status(500).json({ message: 'My Server error' });
}
}

const archiveTask = async (req, res, next) => {
  deleteTaskByIdInner(req,res,false);
}

const getArchiveTasks = async (req, res, next) => {
  try {
   const boardIdCurrent = req.params.id;
    const tasks = await Task.find({ type: 'Archive', boardId: boardIdCurrent }, '-__v');
      res.status(200).json({
        tasks
      });
  } catch(err) {
    res.status(500).json({
      message: 'My Server error',
    });
  }

}

const deleteTaskById = async (req, res, next) => {
  deleteTaskByIdInner(req,res,true);
  }
   async function deleteTaskByIdInner(req,res,isDel) {
    try {
      const taskIdCurrent=req.params.id;
      const url = req.url.split('/')[1];
      let currentTask=await Task.findOne({_id:taskIdCurrent, boardId: url, userId: req.user.userId });
      if(currentTask.taskPrevId!=='') {
      let prevTaskId = (await Task.findOne({_id:currentTask.taskPrevId, boardId: url, userId: req.user.userId }))._id;
      const nextTask = await Task.findByIdAndUpdate(
      { _id: prevTaskId, boardId: url, userId: req.user.userId  },
      { $set:  {taskNextId: currentTask.taskNextId}  }
    );
  }
  if(currentTask.taskNextId!=='') {
    let nextTaskId = (await Task.findOne({_id:currentTask.taskNextId, boardId: url, userId: req.user.userId }))._id;
    const lastTask = await Task.findByIdAndUpdate(
      { _id: nextTaskId, boardId: url, userId: req.user.userId  },
      { $set:  {taskPrevId: currentTask.taskPrevId}  }
    );
   }
     if (isDel){await Task.findByIdAndDelete(taskIdCurrent);}
     else { 
      const TaskCurrent = await Task.findByIdAndUpdate(
      {_id: taskIdCurrent},
      { $set: { type: 'Archive', taskPrevId: '', taskNextId: '' }});}
     countsNumberTaskType(url, req);
      res.status(200).json({
        id: taskIdCurrent
      });
    }catch(error) {
      res.status(500).json({ message: 'My Server error' });
    }
  }

  const updateStatusTask = async (req, res, next) => {
    try {
      const url = req.url.split('/')[1];
      const { taskMoveId, inWhoseInserted } = req.body; 
        const draggableTask = await Task.findOne({_id:taskMoveId, boardId: url, userId: req.user.userId });
        if(draggableTask.taskPrevId===''&& draggableTask.taskNextId!=='') {
          const nextTaskAfterDragTask = await Task.findByIdAndUpdate(
            { _id: draggableTask.taskNextId, boardId: url, userId: req.user.userId },
            { $set:  {taskPrevId: ''}  }
          );
        } else {
          if(draggableTask.taskPrevId!==''){
          const prevTaskBeforeDragTask = await Task.findByIdAndUpdate(
          { _id: draggableTask.taskPrevId, boardId: url, userId: req.user.userId },
          { $set:  {taskNextId: draggableTask.taskNextId}  }
        );}
      }
        if (draggableTask.taskNextId===''&& draggableTask.taskPrevId!=='') {
          const prevTaskBeforeDragTask = await Task.findByIdAndUpdate(
            { _id: draggableTask.taskPrevId, boardId: url, userId: req.user.userId },
            { $set:  {taskNextId: ''}  }
          );
        } else{
          if(draggableTask.taskNextId!==''){
        const nextTaskAfterDragTask = await Task.findByIdAndUpdate(
          { _id: draggableTask.taskNextId, boardId: url, userId: req.user.userId },
          { $set:  {taskPrevId: draggableTask.taskPrevId}  }
        );}
      }
       
      if (inWhoseInserted==='Todo' || inWhoseInserted==='In Progress'
      || inWhoseInserted==='Done') {

        const allTasksWithType =  await Task.find({type:inWhoseInserted, boardId: url, userId: req.user.userId });
        let lastElInArray = allTasksWithType.find((el) => {
          return el.taskNextId===''; 
        });
        let lastTaskLocId; 
        if (lastElInArray!==undefined){
           lastTaskLocId = (await Task.findByIdAndUpdate(
            { _id: lastElInArray._id, boardId: url, userId: req.user.userId },
            { $set:  {taskNextId: taskMoveId}  }
          ))._id;
        } else {
           lastTaskLocId ="";
        }

          const draggableTask = await Task.findByIdAndUpdate(
            { _id: taskMoveId, boardId: url, userId: req.user.userId },
            { $set:  {type: inWhoseInserted,
                     taskPrevId: lastTaskLocId,
                     taskNextId: ''
                    }
            }
          );
          
          countsNumberTaskType(url, req);
          res.status(200).json({ 
              message: 'Task UPDATED successfully',
           });
      } else {
        const inWhoseInsertedTask = await Task.findOne({_id:inWhoseInserted, boardId: url, userId: req.user.userId});
        const draggableTask = await Task.findByIdAndUpdate(
          { _id: taskMoveId, boardId: url, userId: req.user.userId },
          { $set:  {type: inWhoseInsertedTask.type,
                    taskPrevId: inWhoseInsertedTask.taskPrevId,
                    taskNextId: inWhoseInserted
                  }
          });

        const inWhoseInsertedTaskUpdate = await Task.findByIdAndUpdate(
          { _id:inWhoseInserted, boardId: url, userId: req.user.userId },
          { $set:  {
                    taskPrevId: draggableTask._id
                  }
          });
          if (inWhoseInsertedTask.taskPrevId!==''){ 
          const taskBeforeDraggableTask = await Task.findByIdAndUpdate(
            { _id: inWhoseInsertedTask.taskPrevId, boardId: url, userId: req.user.userId },
            { $set:  {
                      taskNextId: draggableTask._id
                    }
            });
          }
          countsNumberTaskType(url, req);
            res.status(200).json({ 
              message: 'Task UPDATED successfully',
           });  
      } 
    } catch(err) {
      res.status(500).json({
        message: 'My Server error',
      });
  }
}

const  changeColorList = async (req, res, next) => {
  try { 
    const {color, type} = req.body;
    if (!color || !type) {
      res.status(400).json({ message: 'The field must not be empty' });
    }
      if(type==='todo') {
        let userCur = await User.findByIdAndUpdate(
          { _id: req.user.userId },
          { $set:  {
            listColorTodo: color,
            typeColumnTodo: type
                  }
          });
      } else if(type==='done') {
        let userCur = await User.findByIdAndUpdate(
          { _id: req.user.userId },
          { $set:  {
            listColorProgress: color,
            typeColumnProgress: type
                  }
          });
      } else {
        let userCur = await User.findByIdAndUpdate(
          { _id: req.user.userId },
          { $set:  {
            listColorDone: color,
            typeColumnDone: type
                  }
          });
      }
      res.status(200).json({ 
        message: `User's list color updated successfully`,
     });

  } catch(err) {
    res.status(500).json({
      message: 'My Server error',
    }); 
}
}

module.exports = {
  createTasks,
  getTasks,
  updateTaskById,
  archiveTask,
  getArchiveTasks,
  deleteTaskById,
  updateStatusTask,
  changeColorList
}



