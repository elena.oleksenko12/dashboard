const { User, userJoiSchema } = require('../models/User');

const getUserProfile = async (req, res, next) => {
  try {
    const {
      _id, username, password
    } = await User.findOne({ _id: req.user.userId });
    res.status(200).json({
      user: {
        _id,
        username,
        password
      },
    });
  } catch (error) {
    res.status(400).json({
      message: 'This user does not exist'
    });
  }
};

async function deleteUser(req, res, next) {
  try {
    const { user } = req;
    await User.deleteOne(user);

    if (!user) {
      res.status(400).json({ message: 'This user does not exist' });
    }
    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).json({
      message: 'Server error',
    });
  }
}

module.exports = {
  getUserProfile,
  deleteUser,
};
