const { Dashboard } = require('../models/Dashboard');
const { Task } = require('../models/Task');
const { Comment } = require('../models/Comment');

async function addComment(req, res, next) {
  try {
    const { body } = req.body;
    const taskIdCurrent= req.params.id;
    const boardIdCurrent = req.url.split('/')[1];
    if (!body) {
      res.status(400).json({ message: 'The field must not be empty' });
    }
    const comment = new Comment({
      body,
     userId: req.user.userId,
     boardId: boardIdCurrent,
     taskId: taskIdCurrent,
    });
   await comment.save();
   res.status(200).json({
    comment
  });
  
  } catch (error) {
    res.status(500).json({
      message: 'My Server error',
    });
  }
}

async function getComments(req, res, next) {
  try {
    const taskIdCurrent= req.params.id;
    const comments = await Comment.find({ taskId: taskIdCurrent }, '-__v');
      res.status(200).json({
        comments
      });
  } catch (err) {
    res.status(500).json({
      message: 'My Server error',
    });
  }
}
const deleteComment = async (req, res, next) => {
  try {
    const taskIdCurrent=req.params.id;
    await Comment.findByIdAndDelete(taskIdCurrent);
    res.status(200).json({
      message: 'Comment deleted successfully',
    });
      } catch(error) {
    res.status(500).json({ message: 'My Server error' });
  }
  }

module.exports = {
    addComment,
    deleteComment,
    getComments
  

}



