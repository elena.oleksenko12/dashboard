const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;
  const user = new User({
    username,
    password: await bcryptjs.hash(password, 10),
  });
  await user.save();
  return await res.status(200).json({                                              
    message: 'Profile created successfully',
  }
  );
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = {
      username: user.username,
      password: user.password,
      userId: user._id,
    };
    const jwtToken = jwt.sign(payload, process.env.secret_jwt_key);
    return res.status(200).json(
      { jwt_token: jwtToken,
      user: user }
      );
  }
  return res.status(401).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
