const { Dashboard } = require('../models/Dashboard');

async function createBoards(req, res, next) {
  try {
    const { name, description } = req.body;
    const board = new Dashboard({
     name,
     description,
     created_by: req.user.userId
    });
   await board.save();
      res.status(200).json(
        {
          board
        },
      );
  } catch (error) {
    res.status(500).json({
      message: 'My Server error',
    });
  }
}

async function getBoards(req, res, next) {
  try {
    const boards = await Dashboard.find({ created_by: req.user.userId }, '-__v');
      res.status(200).json({
        boards,
      });
  } catch (err) {
    res.status(500).json({
      message: 'My Server error',
    });
  }
}

const getBoardById = async (req, res, next) => {
  try {
    const board = await Dashboard.findById(req.params.id);
    res.status(200).json({
      board
    });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      message: 'My Server error',
    });
  }
};

const updateBoardById = async (req, res, next) => {
  try{
  const { name } = req.body;
   const boardNew= await Dashboard.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { name } },
  );
  await boardNew.save();

  const board = await Dashboard.findOne({_id: req.params.id, userId: req.user.userId});
      res.status(200).json({ 
        board
       });
} catch(error) {
  res.status(500).json({ message: 'My Server error' });
}
}

const deleteBoardById = async (req, res, next) => {
  try {
    const deletedDashboard = await Dashboard.findOne({_id: req.params.id});
  await Dashboard.findByIdAndDelete(req.params.id);
    res.status(200).json({
      board: deletedDashboard
    });
  }catch(error) {
    res.status(500).json({ message: 'My Server error' });
  }
  }

module.exports = {
  createBoards,
  getBoards,
  getBoardById,
  updateBoardById,
  deleteBoardById

}
